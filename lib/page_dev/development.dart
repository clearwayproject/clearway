/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:clearway/clash/config.dart';
import 'package:clearway/common.dart';
import 'package:clearway/components/async_widget.dart';
import 'package:clearway/components/auto_scaffold.dart';
import 'package:clearway/conftool.dart';
import 'package:clearway/drawer.dart';
import 'package:clearway/global.dart';
import 'package:clearway/global_res.dart';
import 'package:clearway/main.dart';
import 'package:clearway/page_dev/components.dart';
import 'package:flutter/material.dart';

class DevelopmentPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AutoScaffold(
        appBar: AppBar(title: Text("Development")),
        drawer: DrawerHolder.global,
        body: SizedBox(
          child: Scrollbar(
              child: SingleChildScrollView(
                  child: Column(
            children: [
              MediaQueryCard(),
              SettingsCard(),
              ConfGenCard(),
              ClearWaySubsCard(),
              ClashProcessManagerCard()
            ],
          ))),
        ));
  }
}

class MediaQueryCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context);
    return Card(
      child: Column(
        children: [
          ListTile(
            title: Text("MediaQuery"),
          ),
          SizedBox(
              width: double.infinity,
              child: DataTable(
                columns: [
                  DataColumn(label: Text("Key")),
                  DataColumn(label: Text("Value"))
                ],
                rows: [
                  DataRow(cells: [
                    DataCell(Text("size.width")),
                    DataCell(Text(media.size.width.toString()))
                  ]),
                  DataRow(cells: [
                    DataCell(Text("size.height")),
                    DataCell(Text(media.size.height.toString()))
                  ])
                ],
              ))
        ],
      ),
    );
  }
}

class SettingsCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          ListTile(
            title: Text("Settings Control"),
          ),
          Align(
              alignment: Alignment.centerLeft,
              child: Container(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(children: [
                    AsyncWidget(
                      placeholder: Text("guessClashExePath(): ..."),
                      computation: () => guessClashExePath()
                          .then((value) => Text("guessClashExePath: $value")),
                    )
                  ]))),
          ButtonBar(children: [
            FlatButton(
              child: Text("Clear All"),
              onPressed: () => ClearWaySetings.getInstance()
                  .then((value) => value.clear())
                  .then((value) {
                var message = value ? "Operation Done" : "Operation Failed";
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text(message),
                  duration: const Duration(seconds: 3),
                ));
              }),
            )
          ])
        ],
      ),
    );
  }
}

class ConfGenCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var gen = new ClashConfigGen();
    gen.setLocalProxyPort(7070, 7071, 7072);
    gen.setMode('global');
    gen.addProxy(new ClashVmessConfGen(
        name: "Test",
        remoteAddress: "example.com",
        remotePort: 8964,
        uuid: "ce25bfb9-4637-48a7-96da-5e5281e8d211",
        alterId: 32,
        cipher: "aes256",
        tls: true,
        network: "ws",
        wsPath: "/remote",
        wsHeaders: {
          'VTRAY': "Yes",
        }));
    return Card(
        child: Container(
      child: Column(
        children: [
          ListTile(
            title: Text("Config Generator"),
          ),
          Container(
            child: Align(
              child: Text(gen.result),
              alignment: Alignment.centerLeft,
            ),
            padding: EdgeInsets.only(left: 16, bottom: 16, right: 16),
          )
        ],
      ),
    ));
  }
}

class ClearWaySubsCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          ListTile(
            title: Text("ProxyNodeConf"),
          ),
          Container(
            child: AsyncWidget(
                scope: "proxy-node-conf",
                placeholder: CircularProgressIndicator(),
                computation: () async {
                  var holder = await ProxyNodesConfHolder.get();
                  await holder.load();
                  return Column(
                    children: holder.subscriptions.values
                        .map((e) => Text(e.toString()))
                        .toList(growable: false),
                  );
                }),
          ),
          ButtonBar(
            children: [
              FlatButton(
                child: Text("Delete All Subscription"),
                onPressed: () {
                  ProxyNodesConfHolder.get().then((holder) async {
                    for (var s in holder.subscriptions.keys) {
                      await holder.deleteSubscription(s);
                    }
                    AsyncWidget.refresh(scope: "proxy-node-conf");
                  });
                },
              ),
              FlatButton(
                child: Text("Add Sample Subscriptions"),
                onPressed: () {
                  ProxyNodesConfHolder.get().then((holder) {
                    holder.update(new NodeSubscription("Sample 1", null, [
                      new Node("Sample 1, Node 1", "sample", {}),
                      new Node("Sample 1, Node 2", "sample", {})
                    ]));
                    holder.update(new NodeSubscription("Sample 2", null, [
                      new Node("Sample 2, Node 1", "sample", {}),
                      new Node("Sample 2, Node 2", "sample", {})
                    ]));
                  });
                  AsyncWidget.refresh(scope: "proxy-node-conf");
                },
              ),
              FlatButton(
                child: Text("Force Save All Subscriptions"),
                onPressed: () => ProxyNodesConfHolder.get()
                    .then((holder) => holder.save())
                    .then((_) => AsyncWidget.refresh(scope: "proxy-node-conf"))
                    .then((_) => Scaffold.of(context)
                        .showSnackBar(new SnackBar(content: Text("Saved")))),
              )
            ],
          )
        ],
      ),
    );
  }
}

class ClashProcessManagerCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var resources = ClearWayResources.of(context);
    var clashProcessMan = resources.clashProcessManager;
    return Card(
      child: Column(
        children: [
          ListTile(
            title: Text("ClashProcessManager"),
          ),
          SizedBox(
              width: double.infinity,
              child: TimingRecreateWidget(
                  sleep: const Duration(seconds: 1),
                  builder: (context) => DataTable(
                        columns: [
                          DataColumn(label: Text("Key")),
                          DataColumn(label: Text("Value"))
                        ],
                        rows: [
                          row("currentStatus",
                              clashProcessMan.currentStatus.toString()),
                          row("process", clashProcessMan.process.toString()),
                          row("controller",
                              clashProcessMan.controller.toString()),
                          row(
                              "errorBuffer.toString()",
                              clashProcessMan.canUseErrorBuffer()
                                  ? clashProcessMan.errorBuffer.toString()
                                  : "(error)"),
                          row(
                              "errorBuffer.isEmpty",
                              clashProcessMan.canUseErrorBuffer()
                                  ? clashProcessMan.errorBuffer.isEmpty
                                      .toString()
                                  : "(error)"),
                        ],
                      ))),
          ButtonBar(
            children: [
              ResourceInjector<ClearWayResources>(builder: (resources) {
                var clashProcessMan = resources.clashProcessManager;
                return TimingRecreateWidget(
                    sleep: const Duration(seconds: 1),
                    builder: (ctx) => AsyncWidget(
                          key: UniqueKey(),
                          placeholder: FlatButton(
                            key: UniqueKey(),
                            onPressed: null,
                            child: Text("Please wait..."),
                          ),
                          computation: () async {
                            if (clashProcessMan.currentStatus !=
                                WorkingStatus.NOT_WORKING) {
                              return FlatButton(
                                key: UniqueKey(),
                                child: Text("clashProcessManager.stop()"),
                                onPressed: () {
                                  clashProcessMan.stop();
                                },
                              );
                            } else {
                              return FlatButton(
                                key: UniqueKey(),
                                child: Text("clashProcessManager.start()"),
                                onPressed: () {
                                  clashProcessMan.start();
                                },
                              );
                            }
                          },
                        ));
              })
            ],
          )
        ],
      ),
    );
  }

  DataRow row(String key, String value) {
    return DataRow(
        key: UniqueKey(), cells: [DataCell(Text(key)), DataCell(Text(value))]);
  }
}
