/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'dart:async';

import 'package:clearway/page_settings/tiles.dart';
import 'package:flutter/material.dart';

class TimingRecreateWidget extends StatefulWidget {
  final Duration sleep;
  final SingleValueTransformer<BuildContext, Widget> builder;
  TimingRecreateWidget({Key key, this.sleep, this.builder});

  @override
  State<StatefulWidget> createState() {
    return new TimingRecreateWidgetState(sleep, builder);
  }
}

class TimingRecreateWidgetState extends State<TimingRecreateWidget> {
  final Duration sleep;
  Timer timer;
  SingleValueTransformer<BuildContext, Widget> builder;

  TimingRecreateWidgetState(this.sleep, this.builder);

  bool rebuildLock = false;

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(sleep, (timer) {
      if (!rebuildLock) {
        setState(() {
          rebuildLock = true;
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
    rebuildLock = false;
  }

  @override
  Widget build(BuildContext context) {
    var widget = builder(context);
    rebuildLock = false;
    return widget;
  }
}
