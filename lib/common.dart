/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum WorkingStatus { NOT_WORKING, CONNECTING, WORKING, ERROR }

typedef void SingleValueRecvicer<T>(T value);

class ClearWaySetings {
  static final Logger _logger = new Logger("ClearWaySettings");
  static const CLASH_EXECUTABLE_PATH = 'clash_executable_path';
  static const CONFIGURATION_FOLDER_PATH = 'configuration_folder_path';
  static const SPWAN_METHOD = 'spawn_method';
  static const SELECTED_SUBSCRIPTION = 'selected_subscription';
  static const SELECTED_NODE = 'selected_node';
  static const LOCAL_HTTP_PROXY = 'local_http_proxy';
  static const LOCAL_REDIR_PROXY = 'local_redir_proxy';
  static const LOCAL_SOCKS_PROXY = 'local_socks_proxy';
  static const USE_IPV6 = 'use_ipv6';

  static SharedPreferences pf;

  static Future<SharedPreferences> getInstance() {
    if (pf == null) {
      return SharedPreferences.getInstance().then((value) {
        pf = value;
        return pf;
      });
    } else {
      return new Future.value(pf);
    }
  }

  static Future<T> get<T>(String k) {
    return getInstance().then((pf) => pf.get(k)).then((value) {
      _logger.info("settings get $k = $value");
      return value;
    });
  }

  static Future set<T>(String k, T v) async {
    var pf = await getInstance();
    _logger.info("settings set $k = $v");
    if (v is bool) {
      return pf.setBool(k, v);
    } else if (v is String) {
      return pf.setString(k, v);
    } else if (v is List<String>) {
      return pf.setStringList(k, v);
    } else if (v is double) {
      return pf.setDouble(k, v);
    } else if (v is int) {
      return pf.setInt(k, v);
    } else if (v == null) {
      return pf.remove(k);
    } else {
      throw UnimplementedError();
    }
  }

  static Future<bool> isExists(List<String> keys) {
    return Future.wait(keys.map((k) => get(k))).then((values) {
      for (var x in values) {
        if (x == null) {
          return false;
        }
      }
      return true;
    }).then((v) {
      _logger.info("settings isExists $keys = $v");
      return v;
    });
  }

  static Future<String> getConfigurationFolderPath() =>
      get<String>(CONFIGURATION_FOLDER_PATH);
}

class SettingItemRequired implements Exception {
  final String item;
  SettingItemRequired(this.item);

  @override
  String toString() => "$runtimeType($item)";
}

abstract class ExplainableException implements Exception {
  String get type;
  String get shortMessage;
  String get message;
}

void showTextSnackbar(BuildContext context, String text,
    [SnackBarAction action]) {
  Scaffold.of(context).showSnackBar(new SnackBar(
    content: Text(text),
    action: action,
  ));
}
