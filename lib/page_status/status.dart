/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'dart:async';
import 'package:clearway/clash/status_protocol.dart';
import 'package:clearway/common.dart';
import 'package:clearway/components/async_widget.dart';
import 'package:clearway/components/auto_scaffold.dart';
import 'package:clearway/components/status_fab.dart';
import 'package:clearway/drawer.dart';
import 'package:clearway/global.dart';
import 'package:clearway/global_res.dart';
import 'package:flutter/material.dart';

class StatusPage extends StatelessWidget {
  StatusPage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var clashPM = ClearWayResources.of(context).clashProcessManager;
    return AutoScaffold(
      appBar: AppBar(
        title: Text("Status"),
      ),
      drawer: DrawerHolder.global,
      body: StatusPageBody(),
      floatingActionButton: StatusFAB(clashPM),
    );
  }
}

class StatusPageBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ResourceInjector<ClearWayResources>(
      builder: (resources) => Center(
        child: Container(
          child: Column(
            children: [
              StatusCard(
                statusSource: resources.clashProcessManager,
              ),
              // NodeSuggestionCard(),
              // QuickSettingCard(),
            ],
          ),
        ),
      ),
    );
  }
}

class StatusCard extends StatefulWidget {
  final StatusProtocol statusSource;
  StatusCard({Key key, this.statusSource}) : super(key: key);
  @override
  _StatusCardState createState() =>
      _StatusCardState(statusSource: statusSource);
}

class _StatusCardState extends State<StatusCard> {
  final StatusProtocol statusSource;
  StreamSubscription eventsSubscription;
  WorkingStatus status = WorkingStatus.NOT_WORKING;

  _StatusCardState({this.statusSource});

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    eventsSubscription = statusSource.statusEvents.listen((event) {
      setState(() {
        status = event;
        AsyncWidget.refresh(scope: "status_text");
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    eventsSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    status = ClearWayResources.of(context).clashProcessManager.currentStatus;
    return Card(
      color: getColor(),
      child: Padding(
          padding: EdgeInsets.all(8),
          child: ListTile(
            key: UniqueKey(),
            leading: Icon(getStatusIcon(), size: 48),
            title: Text(getText()),
            subtitle: AsyncWidget(
              scope: "status_text",
              placeholder: SizedBox(
                child: CircularProgressIndicator(),
              ),
              computation: () async {
                var nodeName = await ClearWaySetings.get<String>(
                    ClearWaySetings.SELECTED_NODE);
                return Text(nodeName);
              },
            ),
          )),
    );
  }

  IconData getStatusIcon() {
    switch (status) {
      case WorkingStatus.NOT_WORKING:
        return Icons.face;
      case WorkingStatus.WORKING:
        return Icons.mood;
      case WorkingStatus.CONNECTING:
        return Icons.flight_takeoff;
      case WorkingStatus.ERROR:
        return Icons.error;
    }
    return null;
  }

  String getText() {
    switch (status) {
      case WorkingStatus.NOT_WORKING:
        return "INACTIVE";
      case WorkingStatus.WORKING:
        return "WORKING";
      case WorkingStatus.CONNECTING:
        return "CONNECTING";
      case WorkingStatus.ERROR:
        return "ERROR";
    }
    return null;
  }

  Color getColor() {
    switch (status) {
      case WorkingStatus.NOT_WORKING:
        return Colors.white;
      case WorkingStatus.WORKING:
        return Colors.lightGreen;
      case WorkingStatus.CONNECTING:
        return Colors.lightBlue;
      case WorkingStatus.ERROR:
        return Colors.red;
    }
    return null;
  }
}

class QuickSettingsCard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    throw UnimplementedError();
  }
}
