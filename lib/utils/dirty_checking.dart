/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'dart:async';

import 'package:event_bus/event_bus.dart';
import 'package:logging/logging.dart';

typedef dynamic DirtyCheckClosure();

class DirtyCheckValueChangedEvent<T1, T2> {
  final String name;
  final T1 oldValue;
  final T2 newValue;

  DirtyCheckValueChangedEvent(this.name, this.oldValue, this.newValue);

  DirtyCheckValueChangedEvent<R1, R2> cast<R1 extends T1, R2 extends T2>() =>
      new DirtyCheckValueChangedEvent(this.name, this.oldValue, this.newValue);
}

class DirtyCheckScheduler {
  final _logger = new Logger("DirtyCheckScheduler");
  static DirtyCheckScheduler _scheduler;

  DirtyCheckScheduler._init();

  factory DirtyCheckScheduler() {
    if (_scheduler == null) {
      _scheduler = new DirtyCheckScheduler._init();
    }
    return _scheduler;
  }

  EventBus eventBus = new EventBus();

  Stream<DirtyCheckValueChangedEvent<T1, T2>> on<T1, T2>([String name]) {
    if (name == null) {
      return eventBus.on<DirtyCheckValueChangedEvent<T1, T2>>();
    } else {
      return eventBus
          .on<DirtyCheckValueChangedEvent<T1, T2>>()
          .where((event) => event.name == name);
    }
  }

  Map<String, DirtyCheckClosure> closures = {};
  Map<DirtyCheckClosure, dynamic> values = {};

  void add(String name, DirtyCheckClosure closure) {
    closures[name] = closure;
    values[closure] = null;
    dirtyCheck(name);
  }

  void remove(String name) {
    var f = closures[name];
    closures.remove(name);
    values.remove(f);
  }

  void dirtyCheck(String name) {
    var f = closures[name];
    var oldValue = f();
    var newValue = values[f];
    if (newValue != oldValue) {
      values[f] = oldValue;
      eventBus.fire(new DirtyCheckValueChangedEvent(name, oldValue, newValue));
    }
  }

  Timer timer;
  int checkLock = 0;

  void scheduleDirtyCheck() {
    if (timer != null && timer.isActive) return;
    timer = new Timer.periodic(const Duration(seconds: 1), (timer) {
      if (checkLock > 0) {
        _logger.warning(
            "dirty checking started before is not finished, this time's will be skipped");
        return;
      }
      closures.forEach((key, value) {
        checkLock++;
        scheduleMicrotask(() {
          dirtyCheck(key);
          checkLock--;
        });
      });
    });
    _logger.info("timer have been set");
  }
}
