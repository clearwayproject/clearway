import 'dart:io';

Future<int> calculateDirectorySize(Directory d) async {
  return (await d.stat()).size;
}
