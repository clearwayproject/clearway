/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'dart:async';
import 'dart:io';

import 'package:clearway/common.dart';
import 'package:clearway/components/async_widget.dart';
import 'package:clearway/components/card_preface_title.dart';
import 'package:clearway/utils/clash_seesion.dart';
import 'package:clearway/utils/io_tools.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as _p;

typedef Widget SubPageGenerator(BuildContext context, void close());

class SubPageTile extends StatelessWidget {
  final String title;
  final String description;
  final SubPageGenerator subpageGen;
  SubPageTile({this.title, this.description, this.subpageGen});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListTile(
        title: Text(title),
        subtitle: Text(description),
        onTap: () {
          showDialog(
              context: context,
              builder: (context) => SimpleDialog(
                    title: Text(title),
                    children: [
                      subpageGen(context, () => Navigator.of(context).pop())
                    ],
                  ));
        },
      ),
    );
  }
}

class SettingsPanel extends StatelessWidget {
  final String title;
  final List<Widget> settings;

  SettingsPanel({Key key, this.title, this.settings}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var widgets = <Widget>[];
    for (var w in settings) {
      widgets.add(w);
      widgets.add(Divider(
        thickness: 1.3,
      ));
    }
    widgets.removeLast();
    return Container(
      child: Column(
        children: [
          CardPrefaceTitle(title),
          Container(
            child: Column(
              children: widgets,
            ),
            decoration: BoxDecoration(
                boxShadow: kElevationToShadow[1],
                color: Theme.of(context).cardColor),
          )
        ],
      ),
      padding: EdgeInsets.only(bottom: 8),
    );
  }
}

class NumberSelectorSuggestion<T extends num> {
  final T number;
  final String tip;

  NumberSelectorSuggestion(this.number, [this.tip]);
}

class NumberSelector<T extends num> extends StatefulWidget {
  final T initialValue;
  final List<NumberSelectorSuggestion<T>> suggestions;
  final String label;
  final SingleValueRecvicer<T> onChanged;
  final SingleValueTransformer<T, String> valueCheck;
  NumberSelector(
      {this.initialValue,
      this.suggestions,
      this.label,
      this.onChanged,
      this.valueCheck});

  @override
  _NumberSelectorState<T> createState() {
    return _NumberSelectorState<T>(
        initialValue: initialValue,
        suggestions: suggestions,
        label: label,
        onChanged: onChanged,
        valueCheck: valueCheck);
  }
}

typedef R SingleValueTransformer<T, R>(T value);

class _NumberSelectorState<T extends num> extends State<NumberSelector> {
  final T initialValue;
  final List<NumberSelectorSuggestion<T>> suggestions;
  final String label;
  final SingleValueRecvicer<T> onChanged;
  final SingleValueTransformer<T, String> valueCheck;
  _NumberSelectorState(
      {this.initialValue,
      this.suggestions,
      this.label,
      this.onChanged,
      this.valueCheck}) {
    editingController =
        new TextEditingController(text: initialValue.toString());
  }

  TextEditingController editingController;
  String error;
  bool recovering = false;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Row(
      children: [
        Expanded(
            flex: 1,
            child: Container(
                margin: error != null ? null : EdgeInsets.only(bottom: 22),
                child: TextField(
                  controller: editingController,
                  decoration:
                      InputDecoration(labelText: label, errorText: error),
                  keyboardType: TextInputType.numberWithOptions(),
                  onSubmitted: (value) {
                    checkAndCallback(value);
                  },
                  autofocus: needAutoFoucs(),
                  onChanged: (value) {
                    check(value);
                  },
                ))),
        PopupMenuButton<T>(
          child: Icon(Icons.arrow_drop_down),
          itemBuilder: (context) => suggestions
              .map((e) => new PopupMenuItem<T>(
                    child: Text("${e.number} (${e.tip})"),
                    value: e.number,
                  ))
              .toList(),
          onSelected: (value) {
            setState(() {
              editingController.text = value.toString();
              check(value.toString());
            });
          },
          initialValue: initialValue,
          tooltip: "Show suggestions",
        )
      ],
    ));
  }

  bool check(String value) {
    var number = num.tryParse(value);
    if (number != null) {
      if (number is T) {
        var checkResult = (valueCheck ?? (_) => null)(number);
        if (checkResult != null) {
          setState(() {
            error = checkResult;
            recovering = true;
          });
          return false;
        } else {
          setState(() {
            error = null;
          });
          return true;
        }
      } else {
        setState(() {
          error = "$label requires type $T";
          recovering = true;
        });
        return false;
      }
    } else {
      setState(() {
        error = "Please enter number";
        recovering = true;
      });
      return false;
    }
  }

  void checkAndCallback(String value) {
    if (check(value)) {
      var number = num.tryParse(value);
      onChanged(number);
    }
  }

  bool needAutoFoucs() {
    var result = error != null || recovering;
    if (error == null) recovering = false;
    return result;
  }
}

class NumberSelectorCard<T extends num> extends StatelessWidget {
  final Widget title;
  final Widget description;
  final T initialValue;
  final List<NumberSelectorSuggestion<T>> suggestions;
  final String label;
  final SingleValueRecvicer<T> onChanged;
  final SingleValueTransformer<T, String> valueCheck;
  NumberSelectorCard(
      {this.title,
      this.description,
      this.initialValue,
      this.suggestions,
      this.label,
      this.onChanged,
      this.valueCheck});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 8, right: 16),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: ListTile(
              title: title,
              subtitle: description,
            ),
          ),
          Flexible(
            flex: 1,
            child: NumberSelector(
              initialValue: initialValue,
              suggestions: suggestions,
              label: label,
              onChanged: onChanged,
              valueCheck: valueCheck,
            ),
          )
        ],
      ),
    );
  }
}

Widget sessionCacheStorageTile(BuildContext context) {
  return ListTile(
    title: Text("Session Cache"),
    subtitle: AsyncWidget(
      scope: "setting-session-cache",
      placeholder: Text("Calculating..."),
      computation: () async {
        var configPath = await ClearWaySetings.getConfigurationFolderPath();
        var size = (await Future.wait(await new Directory(configPath)
                .list()
                .where((event) => _p
                    .basename(event.path)
                    .startsWith(ClashSession.SESS_TEMP_NAME))
                .map((event) => calculateDirectorySize(event))
                .toList()))
            .fold<int>(0, (prev, e) => prev + e); // Bit
        return Text("${(size / 8 / 1024)} KB used");
      },
    ),
    trailing: IconButton(
      icon: Icon(Icons.delete_forever),
      tooltip: "Clean Session Cache...",
      onPressed: () {
        showDialog(
            context: context,
            builder: (context) => SimpleDialog(
                  contentPadding: EdgeInsets.all(16),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2))),
                  children: [
                    AsyncWidget(
                      placeholder: Text("Cleaning..."),
                      computation: () async {
                        await Future.wait(await new Directory(
                                await ClearWaySetings
                                    .getConfigurationFolderPath())
                            .list()
                            .where((event) => _p
                                .basename(event.path)
                                .startsWith(ClashSession.SESS_TEMP_NAME))
                            .map((event) => event.delete(recursive: true))
                            .toList());
                        scheduleMicrotask(() {
                          Navigator.of(context).pop();
                        });
                        return Text("Done! This dialog must be closed soon...");
                      },
                    )
                  ],
                )).then((_) => Scaffold.of(context).showSnackBar(
            new SnackBar(content: Text("Session cache are deleted"))));
      },
    ),
  );
}
