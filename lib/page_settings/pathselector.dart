/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:clearway/common.dart';
import 'package:file_chooser/file_chooser.dart';
import 'package:flutter/material.dart';

class PathSelector extends StatefulWidget {
  final SingleValueRecvicer<String> onPathChange;
  final String targetTips;
  final String reason;
  final String rawPath;

  PathSelector(
      {this.onPathChange,
      this.rawPath = '',
      this.targetTips = "file or folder",
      this.reason});

  @override
  _PathSelectorState createState() => new _PathSelectorState(
        this.onPathChange,
        this.rawPath,
        this.targetTips,
        this.reason,
      );
}

class _PathSelectorState extends State<PathSelector> {
  final SingleValueRecvicer<String> onPathChange;
  final String targetTips;
  final String reason;
  String path;
  TextEditingController _controller;

  _PathSelectorState(this.onPathChange, this.path, this.targetTips,
      [this.reason]) {
    _controller = new TextEditingController(text: path);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Flexible(
          child: TextField(
            controller: _controller,
            decoration: InputDecoration(
                labelText:
                    "Path of $targetTips" + (reason != null ? " $reason" : "")),
            onSubmitted: onPathChange,
          ),
        ),
        Flexible(
          flex: 0,
          child: IconButton(
            icon: Icon(Icons.view_list),
            padding: EdgeInsets.only(top: 24),
            tooltip: "Choose...",
            onPressed: () {
              showOpenPanel(
                canSelectDirectories: (targetTips.contains("folder") ||
                    targetTips.contains("directory")),
              ).then((result) {
                if (!result.canceled) {
                  setState(() {
                    _controller.text = result.paths[0];
                  });
                  onPathChange(result.paths[0]);
                }
              });
            },
          ),
        )
      ],
    );
  }
}

class PathSelectorCard extends StatelessWidget {
  final Widget title;
  final Widget description;
  final SingleValueRecvicer<String> onPathChange;
  final String targetTips;
  final String rawPath;
  final String reason;
  PathSelectorCard(this.title, this.description, this.onPathChange,
      {this.targetTips, this.reason, this.rawPath});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          ListTile(
            title: title,
            subtitle: description,
          ),
          Container(
            child: PathSelector(
              onPathChange: onPathChange,
              targetTips: targetTips,
              reason: reason,
              rawPath: rawPath,
            ),
            padding: EdgeInsets.only(left: 16, right: 16),
          )
        ],
      ),
      padding: EdgeInsets.only(bottom: 16),
    );
  }
}
