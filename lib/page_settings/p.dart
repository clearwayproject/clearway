/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:clearway/common.dart';
import 'package:clearway/components/async_widget.dart';
import 'package:clearway/components/fixed_option_selector.dart';
import 'package:clearway/global_res.dart';
import 'package:clearway/static_config.dart';
import 'pathselector.dart';
import 'package:flutter/material.dart';

import 'tiles.dart';

Widget generatePathSelectorItem(String title, String subtitle, String key,
    {String targetTips, String reason}) {
  return AsyncWidget(
    placeholder: Container(
      child: ListTile(
        title: Text(title),
        subtitle: Text(subtitle),
        trailing: Text("Please wait..."),
      ),
    ),
    computation: () async {
      var value = await ClearWaySetings.get<String>(key);
      return PathSelectorCard(
        Text(title),
        Text(subtitle),
        (newValue) {
          ClearWaySetings.set(key, newValue);
        },
        targetTips: targetTips,
        rawPath: value,
        reason: reason,
      );
    },
  );
}

typedef String Stringify<T>(T value);

Widget generateFixedOptionSelectorForOneItem<T, P>(
    String title,
    String subtitle,
    String key,
    List<T> items,
    Stringify stringify,
    T initalLookup(P value),
    P storeConvertor(T value),
    {void additionalCallback(P value)}) {
  return AsyncWidget(
    placeholder: ListTile(
      title: Text(title),
      subtitle: Text(subtitle),
      trailing: Text("Please wait..."),
    ),
    computation: () async {
      var value = await ClearWaySetings.get<P>(key);
      return FixedOptionSelectorCard<T>(
        title: Text(title),
        description: Text(subtitle),
        builder: (item) => Text(stringify(item)),
        items: items,
        mutiple: false,
        initialSelected: [initalLookup(value)],
        onSelectedChanged: (state) {
          var val = storeConvertor(state.selected[0]);
          ClearWaySetings.set(key, val);
          additionalCallback(val);
        },
      );
    },
  );
}

Widget generateNumberSelectorItem<T extends num>(
    String title,
    String subtitle,
    String key,
    String label,
    List<NumberSelectorSuggestion<T>> suggestions,
    SingleValueTransformer<T, String> valueCheck) {
  return AsyncWidget(
    placeholder: ListTile(
      title: Text(title),
      subtitle: Text(subtitle),
      trailing: CircularProgressIndicator(),
    ),
    computation: () async {
      var value = await ClearWaySetings.get<T>(key);
      return NumberSelectorCard<T>(
        title: Text(title),
        description: Text(subtitle),
        initialValue: value,
        suggestions: suggestions,
        label: label,
        valueCheck: valueCheck,
        onChanged: (value) {
          ClearWaySetings.set(key, value);
        },
      );
    },
  );
}

Widget clashExePathSelector() {
  return generatePathSelectorItem(
      "Your Clash Executable",
      "You may tell ClearWay to use a determinded clash executable to run as proxy service",
      ClearWaySetings.CLASH_EXECUTABLE_PATH,
      targetTips: 'executable');
}

Widget configurationFolderPathSelector() {
  return generatePathSelectorItem(
      "Configuration Folder",
      "ClearWay use the folder to store your data, such as nodes and rules",
      ClearWaySetings.CONFIGURATION_FOLDER_PATH,
      targetTips: "folder");
}

Widget spawnMethodSelector() {
  var items = [
    {'name': 'Process (dart:io)', 'key': 'dartio_platform'},
  ];
  if (SPAWN_METHOD_FORK_AND_SPAWN) {
    items.add({'name': 'fork & spawn', 'key': 'native_fork'});
  }
  return ResourceInjector<ClearWayResources>(
      builder: (resources) =>
          generateFixedOptionSelectorForOneItem<Map<String, String>, String>(
              "Spawn Method",
              "ClearWay will use the way you decided to spawn proxy service",
              ClearWaySetings.SPWAN_METHOD,
              items,
              (value) => value['name'], (s) {
            if (s == null) {
              ClearWaySetings.set(
                  ClearWaySetings.SPWAN_METHOD, items[0]['key']);
              return items[0];
            }
            return items.firstWhere((element) => element['key'] == s);
          }, (map) => map['key'],
              additionalCallback: (v) =>
                  resources.clashProcessManager.spawnMethod = v));
}

Widget localProxyPortHTTPSelector() {
  return generateNumberSelectorItem<int>(
      "Local HTTP Proxy Port",
      "Activte a proxy with HTTP Protocol on the port",
      ClearWaySetings.LOCAL_HTTP_PROXY,
      "Port", [
    new NumberSelectorSuggestion(8080, "Default Port of HTTP Proxy"),
    new NumberSelectorSuggestion(7890, "Default HTTP Proxy Port in Clash")
  ], (value) {
    if (value <= 0 || value >= 65535) {
      return "Port is greater than 0 and less than 65535";
    } else {
      return null;
    }
  });
}

Widget localProxyPortSocksSelector() {
  return generateNumberSelectorItem<int>(
      "Local Socks Proxy Port",
      "Activte a proxy with Socks5 Protocol on the port",
      ClearWaySetings.LOCAL_SOCKS_PROXY,
      "Port",
      [new NumberSelectorSuggestion(7891, "Default Socks Proxy Port in Clash")],
      (value) {
    if (value <= 0 || value >= 65535) {
      return "Port is greater than 0 and less than 65535";
    } else {
      return null;
    }
  });
}

Widget localProxyPortRedirSelector() {
  return generateNumberSelectorItem<int>(
      "Local Transparent Proxy Port",
      "Activte a proxy used with transparent proxy on the port",
      ClearWaySetings.LOCAL_REDIR_PROXY,
      "Port", [
    new NumberSelectorSuggestion(
        7892, "Default Transparent Proxy Port in Clash")
  ], (value) {
    if (value <= 0 || value >= 65535) {
      return "Port is greater than 0 and less than 65535";
    } else {
      return null;
    }
  });
}

Widget useIPv6Selector() {
  return ListTile(
    title: Text("Use IPv6"),
    trailing: AsyncWidget(
      scope: "settings-ipv6",
      placeholder: CircularProgressIndicator(),
      computation: () async {
        var useIPv6 = await ClearWaySetings.get<bool>(ClearWaySetings.USE_IPV6);
        return Checkbox(
          value: useIPv6,
          onChanged: (v) {
            ClearWaySetings.set(ClearWaySetings.USE_IPV6, v);
            AsyncWidget.refresh(scope: "settings-ipv6");
          },
        );
      },
    ),
  );
}
