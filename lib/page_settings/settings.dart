/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:clearway/common.dart';
import 'package:clearway/components/async_widget.dart';
import 'package:clearway/components/auto_scaffold.dart';
import 'package:clearway/drawer.dart';
import 'p.dart';
import 'tiles.dart';
import 'package:flutter/material.dart';

class SettingsPage extends StatelessWidget {
  SettingsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AutoScaffold(
      appBar: AppBar(
        title: Text("Settings"),
      ),
      drawer: DrawerHolder.global,
      body: Scrollbar(child: SingleChildScrollView(child: SettingsList())),
    );
  }
}

class SettingsList extends StatelessWidget {
  SettingsList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SettingsPanel(
          title: "General",
          settings: [
            configurationFolderPathSelector(),
          ],
        ),
        SettingsPanel(
          title: "Proxy Client",
          settings: [
            clashExePathSelector(),
            spawnMethodSelector(),
            localProxyPortHTTPSelector(),
            localProxyPortSocksSelector(),
            localProxyPortRedirSelector(),
            useIPv6Selector(),
          ],
        ),
        SettingsPanel(
          title: "Storage",
          settings: [
            sessionCacheStorageTile(context),
          ],
        ),
        SizedBox(height: 16),
        Container(
            decoration: BoxDecoration(
              boxShadow: kElevationToShadow[1],
              color: Theme.of(context).cardColor,
            ),
            child: AboutListTile(
              applicationName: "ClearWay",
              applicationLegalese: """
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.""",
            )),
        SizedBox(
          height: 48,
        )
      ],
    );
  }
}
