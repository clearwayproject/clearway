/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:clearway/app.dart';
import 'package:clearway/common.dart';
import 'package:clearway/conftool.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:path_provider/path_provider.dart';
import 'static_config.dart';

final Logger _logger = new Logger("main.dart");

Future<String> guessPath(List<String> templates, String fileName) async {
  for (var t in templates) {
    var path = t.replaceAll('?', fileName);
    var f = new File(path);
    try {
      if (await f.exists()) {
        return path;
      }
    } catch (_) {/* ignore */}
  }
  return null;
}

Future<String> guessConfPath() async {
  var dir = await getApplicationSupportDirectory();
  if (await dir.exists()) {
    return dir.path;
  } else {
    try {
      await dir.create();
    } catch (e, st) {
      _logger.info("guessConfPath(): error while creating directory", e, st);
      return null;
    }
    return dir.path;
  }
}

Future<String> guessClashExePath() {
  return guessPath(['/bin/?', '/usr/local/bin/?', '~/.local/bin/?'], 'clash');
}

class InitApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          body: Center(
        child: Column(
          children: [
            LinearProgressIndicator(),
            Container(
              child: Text(
                "PLEASE STAND BY",
                textScaleFactor: 1.75,
              ),
              padding: EdgeInsets.all(8),
            )
          ],
        ),
      )),
    );
  }
}

class ErrorApp extends StatelessWidget {
  final dynamic error;
  final StackTrace stackTrace;
  ErrorApp(this.error, this.stackTrace);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          body: Center(
        child: Column(
          children: [
            Container(
              child: Text(
                "An Error Happened During ClearWay Starting",
                textScaleFactor: 1.75,
              ),
              padding: EdgeInsets.all(8),
            ),
            Text(
              error.toString(),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(stackTrace.toString()),
          ],
        ),
      )),
    );
  }
}

main() {
  Logger.root.onRecord.listen((record) {
    print(
        '${record.level.name}: ${record.loggerName} ${record.time}: ${record.message}');
  });
  if (DEBUG) {
    Logger.root.level = Level.ALL;
    Logger.root.warning("verbose logging enabled");
  }
  Future.wait([
    ClearWaySetings.isExists([ClearWaySetings.CONFIGURATION_FOLDER_PATH]),
    ClearWaySetings.isExists([ClearWaySetings.CLASH_EXECUTABLE_PATH]),
    new Future(() async {
      var folderp =
          await ClearWaySetings.get(ClearWaySetings.CONFIGURATION_FOLDER_PATH);
      if (folderp != null) {
        return await (new Directory(folderp)).exists();
      } else {
        return false;
      }
    }),
    ClearWaySetings.isExists([ClearWaySetings.LOCAL_HTTP_PROXY]),
    ClearWaySetings.isExists([ClearWaySetings.LOCAL_SOCKS_PROXY]),
    ClearWaySetings.isExists([ClearWaySetings.LOCAL_REDIR_PROXY]),
    ClearWaySetings.isExists([ClearWaySetings.USE_IPV6]),
  ])
      .then((values) async {
        if (!(values.reduce((value, element) => value && element))) {
          _logger.info("INIT.");
          runApp(InitApp());
          if (!values[0]) {
            await ClearWaySetings.set(ClearWaySetings.CONFIGURATION_FOLDER_PATH,
                await guessConfPath());
          }
          if (!values[1]) {
            await ClearWaySetings.set(ClearWaySetings.CLASH_EXECUTABLE_PATH,
                await guessClashExePath());
          }
          if (!values[2]) {
            var folderp = await ClearWaySetings.get<String>(
                ClearWaySetings.CONFIGURATION_FOLDER_PATH);
            await (new Directory(folderp)).create();
            _logger.info("folder $folderp created");
          }
          if (!values[3]) {
            await ClearWaySetings.set(ClearWaySetings.LOCAL_HTTP_PROXY, 7890);
          }
          if (!values[4]) {
            await ClearWaySetings.set(ClearWaySetings.LOCAL_SOCKS_PROXY, 7891);
          }
          if (!values[5]) {
            await ClearWaySetings.set(ClearWaySetings.LOCAL_REDIR_PROXY, 7892);
          }
          if (!values[6]) {
            await ClearWaySetings.set(ClearWaySetings.USE_IPV6,
                false); // TODO: guess user if have ipv6
          }
        }
      })
      .then((_) => ProxyNodesConfHolder.get())
      .then((conf) async {
        conf.rootPath = await ClearWaySetings.get(
            ClearWaySetings.CONFIGURATION_FOLDER_PATH);
        return conf.load();
      })
      .then((_) => _logger.info("nodes loaded."))
      .then((_) {
        if (!KEEP_IN_INIT_APP) runApp(ClearWayApp());
      });
}
