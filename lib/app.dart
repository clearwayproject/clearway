/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'dart:async';

import 'package:clearway/common.dart';
import 'package:clearway/conftool.dart';
import 'package:clearway/drawer.dart';
import 'package:clearway/page_dev/development.dart';
import 'package:clearway/page_nodes/nodes.dart';
import 'package:clearway/page_settings/settings.dart';
import 'package:clearway/page_status/status.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';

import 'clash/manger.dart';
import 'components/async_widget.dart';
import 'global_res.dart';

class ClearWayApp extends StatelessWidget {
  final _logger = new Logger("ClearWayApp");
  @override
  Widget build(BuildContext context) {
    return ClearWayResources(
        key: GlobalKey(debugLabel: "resources-provider"),
        clashProcessManager: new ClashProcessManager(),
        proxyNodesConf: ProxyNodesConfHolder.getSync(),
        child: ResourceInjector<ClearWayResources>(
            builder: (resources) => AsyncWidget(
                placeholder: Placeholder(
                  fallbackHeight: 600,
                  fallbackWidth: 960,
                  color: Colors.white,
                ),
                computation: () async {
                  var binaryPath = await ClearWaySetings.get<String>(
                      ClearWaySetings.CLASH_EXECUTABLE_PATH);
                  var type = await ClearWaySetings.get<String>(
                      ClearWaySetings.SPWAN_METHOD);
                  var clashPM = resources.clashProcessManager;
                  clashPM.binaryPath = binaryPath;
                  clashPM.renewProcess(type);
                  _logger.info(
                      "clash process manager's important fields have been set");
                  return MaterialApp(
                    title: 'ClearWay',
                    theme: ThemeData(
                        primarySwatch: Colors.blue,
                        visualDensity: VisualDensity.adaptivePlatformDensity),
                    showPerformanceOverlay: false,
                    showSemanticsDebugger: false,
                    debugShowCheckedModeBanner: false,
                    routes: {
                      '/status': (context) => StatusPage(),
                      '/settings': (context) => SettingsPage(),
                      '/nodes': (context) => NodePage(),
                      '/development': (context) => DevelopmentPage(),
                    },
                    initialRoute: '/status',
                    navigatorObservers: [DrawerNavigatorObserver()],
                  );
                })));
  }
}

class SimplePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Test Page")),
      drawer: DrawerHolder.global,
    );
  }
}
