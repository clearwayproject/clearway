/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:clearway/components/fixed_option_selector.dart';
import 'package:clearway/conftool.dart';
import 'common.dart';
import 'package:flutter/material.dart';

typedef Widget DialogBuilder(BuildContext ctx);
typedef void SetStateDef(void fn());

class AddNodeDialogBlueprint implements Blueprint {
  NodeSubscription subscription;
  List<Map<String, String>> acceptedTypies;
  String selected;

  AddNodeDialogBlueprint(
      {this.acceptedTypies, this.selected, this.subscription});

  Widget build(BuildContext ctx) {
    return AlertDialog(
      title: Text("Add node to \"${subscription.name}\""),
      content: ListTile(
        title: Text("Type"),
        trailing: FittedBox(
            child: FixedOptionSelector(
          items: acceptedTypies,
          mutiple: false,
          builder: (item) {
            return Text(item['text']);
          },
          initialSelected: [
            acceptedTypies.firstWhere((element) => selected == element['type'])
          ],
          onSelectedChanged: (state) {
            selected = state.selected[0]['type'];
          },
        )),
      ),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(2))),
      actions: [
        FlatButton(
          child: Text("Cancel"),
          onPressed: () => Navigator.of(ctx).pop(null),
        ),
        FlatButton(
          child: Text("OK"),
          onPressed: () => Navigator.of(ctx).pop(selected),
        ),
      ],
    );
  }
}
