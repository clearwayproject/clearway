/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:clearway/conftool.dart';

String _getName(String base, int pointer) {
  return pointer == 0 ? base : "${base}_$pointer";
}

String getUniqueNodeName(List<Node> nodes) {
  String base = "Undefined";
  int pointer = 0;
  while (nodes.any((element) => _getName(base, pointer) == element.name))
    pointer++;
  return _getName(base, pointer);
}
