/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:clearway/common.dart';
import 'package:clearway/components/async_widget.dart';
import 'package:clearway/components/card_preface_title.dart';
import 'package:clearway/conftool.dart';
import 'package:clearway/global.dart';
import 'package:clearway/global_res.dart';
import 'package:clearway/static_config.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';

import 'dlg_add_node.dart';
import 'node_editor.dart';
import 'node_new_unique_name.dart';
import 'subs_list.dart';

class SubscriptionNodeList extends StatefulWidget {
  final NodeSubscription subscription;
  SubscriptionNodeList(this.subscription, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SubscriptionNodeListState(subscription);
  }
}

class SubscriptionNodeListState extends State<SubscriptionNodeList> {
  final _logger = new Logger("SubscriptionNodeListState");
  NodeSubscription subscription;
  List<NodeExpansion> nodes;
  NodeSubscription newSubscription;

  SubscriptionNodeListState(this.subscription) {
    newSubscription = subscription.clone();
  }

  @override
  void initState() {
    super.initState();
    nodes = subscription.nodes
        .map((e) => new NodeExpansion(e))
        .toList(growable: false);
  }

  @override
  void didUpdateWidget(SubscriptionNodeList oldWidget) {
    super.didUpdateWidget(oldWidget);
    nodes = subscription.nodes
        .map((e) => new NodeExpansion(e))
        .toList(growable: false);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            CardPrefaceTitle(subscription.name),
            SizedBox(
              width: 4,
            ),
            Expanded(
                child: ButtonBar(children: [
              IconButton(
                padding: EdgeInsets.only(top: 8),
                icon: Icon(Icons.playlist_add),
                tooltip: "Add node to ${subscription.name}...",
                onPressed: () {
                  final acceptedTypies = [
                    {'type': 'vmess', 'text': "Vmess"}
                  ];
                  if (DEBUG) {
                    acceptedTypies.add({'type': 'sample', 'text': "Sample"});
                    acceptedTypies.add({'type': 'unknown', 'text': "Unkown"});
                  }
                  String selected = 'vmess';
                  AddNodeDialogBlueprint blueprint = new AddNodeDialogBlueprint(
                      acceptedTypies: acceptedTypies,
                      selected: selected,
                      subscription: subscription);
                  showDialog<String>(
                    context: context,
                    builder: blueprint.build,
                  ).then((value) async {
                    if (value != null) {
                      var node = new Node(
                          getUniqueNodeName(subscription.nodes), value, {});
                      newSubscription.nodes.add(node);
                      var holder = await ProxyNodesConfHolder.get();
                      holder.update(subscription);
                      setState(() {
                        subscription = newSubscription;
                        newSubscription = subscription.clone();
                      });
                    }
                  });
                },
              ),
              IconButton(
                padding: EdgeInsets.only(top: 8),
                icon: Icon(Icons.delete_forever),
                tooltip: "Delete subscription '${subscription.name}'",
                onPressed: () {
                  ProxyNodesConfHolder.get().then((holder) async {
                    _logger.info(() => "delete $subscription");

                    await holder.deleteSubscription(subscription.name);
                    Scaffold.of(context).showSnackBar(new SnackBar(
                      content: Text("'${subscription.name}' have been deleted"),
                      action: SnackBarAction(
                        label: "UNDO",
                        onPressed: () {
                          holder.update(subscription);
                          holder.save().whenComplete(() {
                            var event =
                                new SubscriptionChanged(null, newSubscription);
                            eventBus.fire(event);
                          });
                        },
                      ),
                    ));
                    eventBus.fire(new SubscriptionChanged(subscription, null));
                  });
                },
              ),
              IconButton(
                padding: EdgeInsets.only(top: 8),
                icon: Icon(Icons.edit),
                tooltip: "Edit ${subscription.name}",
                onPressed: null,
              )
            ]))
          ],
        ),
        generateExpansionPanelList(),
      ],
    );
  }

  ExpansionPanelList generateExpansionPanelList() {
    var panels = nodes
        .map((e) => ExpansionPanel(
            headerBuilder: (context, isExpanded) => ListTile(
                  title: Text(e.node.name),
                  leading: NodeRadio(subscription, e.node),
                ),
            body: NodeEditor(
                new NodeEditingPresent(subscription.name, e.node.name)),
            isExpanded: e.isExpanded))
        .toList();
    return ExpansionPanelList(
      children: panels,
      expansionCallback: (index, isExpanded) {
        setState(() {
          nodes[index].isExpanded = !isExpanded;
        });
      },
    );
  }
}

class NodeExpansion {
  final Node node;
  bool isExpanded = false;

  NodeExpansion(this.node);
}

class NodeRadio extends StatefulWidget {
  final NodeSubscription subscription;
  final Node node;
  NodeRadio(this.subscription, this.node);

  @override
  _NodeRadioState createState() =>
      _NodeRadioState(this.subscription, this.node);
}

class _NodeRadioState extends State<NodeRadio> {
  final NodeSubscription subscription;
  final Node node;
  String combinedName;
  bool isSelected = false;
  _NodeRadioState(this.subscription, this.node) {
    combinedName = subscription.name + "." + node.name;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AsyncWidget(
      placeholder: CircularProgressIndicator(),
      scope: "node_radio",
      computation: () async {
        var selectedSubscription = await ClearWaySetings.get<String>(
            ClearWaySetings.SELECTED_SUBSCRIPTION);
        var selectedNode =
            await ClearWaySetings.get<String>(ClearWaySetings.SELECTED_NODE);
        var selectedName = "$selectedSubscription.$selectedNode";
        return Radio(
          value: combinedName,
          groupValue: selectedName,
          toggleable: !isSelected,
          onChanged: (_) {
            Future.wait([
              ClearWaySetings.set(
                  ClearWaySetings.SELECTED_SUBSCRIPTION, subscription.name),
              ClearWaySetings.set(ClearWaySetings.SELECTED_NODE, node.name)
            ]).then((_) async {
              AsyncWidget.refresh(scope: "node_radio");
              var clashPM = ClearWayResources.of(context).clashProcessManager;
              if (clashPM.currentStatus != WorkingStatus.NOT_WORKING) {
                await clashPM.restart();
              }
            });
          },
        );
      },
    );
  }
}
