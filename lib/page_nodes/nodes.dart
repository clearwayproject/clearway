/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'dart:async';

import 'package:clearway/clash/config.dart';
import 'package:clearway/common.dart';
import 'package:clearway/components/async_widget.dart';
import 'package:clearway/components/auto_scaffold.dart';
import 'package:clearway/components/fixed_option_selector.dart';
import 'package:clearway/components/status_fab.dart';
import 'package:clearway/conftool.dart';
import 'package:clearway/drawer.dart';
import 'package:clearway/global.dart';
import 'package:clearway/global_res.dart';
import 'package:clearway/page_nodes/dlg_add_subs.dart';
import 'package:flutter/material.dart';

import 'subs_list.dart';
import 'components/stateful_linear_progress.dart';

class NodePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var clashPM = ClearWayResources.of(context).clashProcessManager;
    return AutoScaffold(
      appBar: AppBar(
        title: Text("Nodes"),
        actions: [
          IconButton(
              tooltip: "Reload subscriptions and nodes from file system",
              icon: Icon(Icons.refresh),
              onPressed: () => ProxyNodesConfHolder.get()
                  .then((holder) => holder.load())
                  .then((_) => AsyncWidget.refresh())),
          IconButton(
            icon: Icon(Icons.cloud_download),
            tooltip: "Download remote subscriptions' nodes",
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) {
                  var progKey = GlobalKey<StatefulLinearProgressIndicatorState>(
                      debugLabel: "progress-show");
                  return AlertDialog(
                    title: Text("Downloading"),
                    content: AsyncWidget(
                      placeholder: StatefulLinearProgressIndicator(
                        key: progKey,
                      ),
                      computation: () async {
                        progKey.currentState.setValue(0);
                        var subscriptions = ProxyNodesConfHolder.getSync()
                            .collectSubscriptionHasSource();
                        var subsCount = subscriptions.length + 1e-10;
                        var doneCount = 0 + 1e-10;
                        for (var subs in subscriptions) {
                          var source = new ClashProxiesSource(subs.source);
                          var nodes = await source.getNodesFromRemote();
                          subs.nodes.clear();
                          subs.nodes.addAll(nodes);
                          progKey.currentState.setValue(doneCount / subsCount);
                        }
                        scheduleMicrotask(() {
                          Navigator.of(context).pop();
                        });
                        await ProxyNodesConfHolder.getSync().save();
                        return Text("Done! This dialog must be closed soon.");
                      },
                    ),
                  );
                },
              );
            },
          ),
          AddSubscriptionIconButton(),
        ],
      ),
      drawer: DrawerHolder.global,
      floatingActionButton: StatusFAB(clashPM),
      body: Scrollbar(
          child: SingleChildScrollView(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  AsyncSubscriptionListWarpper(key: UniqueKey()),
                  SizedBox(
                    height: 96,
                  )
                ],
              ))),
    );
  }
}
