/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'dart:async';

import 'package:clearway/conftool.dart';
import 'package:clearway/global.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';

import 'node_list.dart';

class SubscriptionKey implements Key {
  final String name;
  SubscriptionKey(this.name);

  @override
  operator ==(other) {
    return (other is SubscriptionKey) && other.name == this.name;
  }

  @override
  int get hashCode => super.hashCode;
}

class SubscriptionChanged {
  final NodeSubscription oldSubscription;
  final NodeSubscription newSubscription;
  Completer notifier = new Completer();

  SubscriptionChanged(this.oldSubscription, this.newSubscription);
}

class AsyncSubscriptionListWarpper extends StatefulWidget {
  final Key key;
  AsyncSubscriptionListWarpper({this.key}) : super(key: key);

  @override
  AsyncSubscriptionListWarpperState createState() =>
      new AsyncSubscriptionListWarpperState();
}

class AsyncSubscriptionListWarpperState
    extends State<AsyncSubscriptionListWarpper> {
  final Logger _logger = new Logger("AsyncSubscriptionListWarpperState");
  List<NodeSubscription> subscriptions;
  List<Widget> widgets;
  bool flagDone = false;

  AsyncSubscriptionListWarpperState();

  StreamSubscription evSubscription;

  @override
  void initState() {
    super.initState();
    listenSubscriptionChangedEvent();
    if (!flagDone) performUIUpdate();
  }

  void listenSubscriptionChangedEvent() {
    evSubscription = eventBus.on<SubscriptionChanged>().listen((event) {
      _logger.finest("subscription changed with event ${event.hashCode}");
      ProxyNodesConfHolder.get()
          .then((value) async {
            if (event.newSubscription == null) {
              await value.deleteSubscription(event.oldSubscription.name);
            } else {
              if (event.newSubscription?.name != event.oldSubscription?.name &&
                  event.oldSubscription != null) {
                await value.deleteSubscription(event.oldSubscription.name);
              }
              value.update(event.newSubscription);
              await value.save();
            }
          })
          .whenComplete(event.notifier.complete)
          .whenComplete(() => performUIUpdate());
    });
  }

  void performUIUpdate() {
    _logger.fine("perform update");
    ProxyNodesConfHolder.get().then((conf) {
      return conf.subscriptions;
    }).then((subscriptions) {
      this.subscriptions = subscriptions.values.toList();
      this.subscriptions.sort();
      refreshUI();
      setState(() {
        flagDone = true;
      });
    });
  }

  void refreshUI() {
    setState(() =>
        widgets = this.subscriptions.map((e) => createNodeList(e)).toList());
  }

  Widget createNodeList(NodeSubscription subs) {
    return SubscriptionNodeList(
      subs,
      key: new SubscriptionKey(subs.name),
    );
  }

  @override
  void dispose() {
    super.dispose();
    evSubscription.cancel();
    widgets = null;
    flagDone = false;
  }

  @override
  Widget build(BuildContext context) {
    if (flagDone) {
      return Column(children: widgets);
    } else {
      return LinearProgressIndicator();
    }
  }
}
