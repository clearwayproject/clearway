/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:clearway/components/async_widget.dart';
import 'package:clearway/components/fixed_option_selector.dart';
import 'package:clearway/conftool.dart';
import 'package:clearway/page_settings/tiles.dart';
import 'package:flutter/material.dart';
import 'package:json2yaml/json2yaml.dart';
import 'package:yaml/yaml.dart';

import 'components/text_form_selector.dart';

Future<NodeSubscription> lookupSubscription(String name) async {
  var nodeConf = await ProxyNodesConfHolder.get();
  return nodeConf.subscriptions[name];
}

Future<Node> lookupNodeConf(String subscriptionName, String nodeName) async {
  var subscription = await lookupSubscription(subscriptionName);
  return subscription.nodes.firstWhere((element) => element.name == nodeName);
}

class NodeEditingPresent {
  String subscriptionName;
  String nodeName;

  NodeEditingPresent(this.subscriptionName, this.nodeName);
}

class NodeEditor extends StatefulWidget {
  final NodeEditingPresent present;
  NodeEditor(this.present);

  @override
  _NodeEditorState createState() => _NodeEditorState(present);
}

class _NodeEditorState extends State<NodeEditor> {
  final NodeEditingPresent present;
  _NodeEditorState(this.present);

  @override
  Widget build(BuildContext context) {
    return AsyncWidget(
      placeholder: LinearProgressIndicator(),
      computation: () async {
        var subs = await lookupSubscription(present.subscriptionName);
        var node = subs.nodes
            .firstWhere((element) => element.name == present.nodeName);
        if (node.type == "sample") {
          return Container(
            padding: EdgeInsets.all(8),
            child: Text(
                "This is a sample node used for developing, it could not be edited"),
          );
        } else if (node.type == "vmess") {
          return VmessNodeEditor(present);
        } else {
          return Padding(
              padding: EdgeInsets.all(16),
              child: Flex(
                direction: Axis.vertical,
                children: [
                  Text(
                      "Could not find avaliable editor for type ${node.type}."),
                  Text("Supported type(s): sample and vmess."),
                  Text(
                      "Something wrong may have happened, or just something unsupported yet."),
                  Text(
                      "If this message keeps always, please contract maintainer.")
                ],
              ));
        }
      },
    );
  }
}

class VmessNodeEditor extends StatelessWidget {
  final NodeEditingPresent present;
  VmessNodeEditor(this.present);
  final formKey = new GlobalKey<FormState>();
  final asyncKey = new GlobalKey<AsyncWidgetState>();

  @override
  Widget build(BuildContext context) {
    return AsyncWidget(
      key: asyncKey,
      placeholder: CircularProgressIndicator(),
      computation: () async {
        var node =
            await lookupNodeConf(present.subscriptionName, present.nodeName);
        return Container(
            padding: EdgeInsets.only(left: 8, right: 8, bottom: 8),
            child: Form(
              key: formKey,
              autovalidate: true,
              child: Column(
                children: [
                  Flex(direction: Axis.horizontal, children: [
                    Expanded(
                        flex: 4,
                        child: TextFormField(
                          initialValue: node.name,
                          decoration: InputDecoration(labelText: "Name"),
                          onSaved: (value) => node.name = value,
                        )),
                    Flexible(
                      flex: 1,
                      child: WithLabel(
                        labelText: "Type",
                        padding: EdgeInsets.only(left: 16, top: 8),
                        child: Align(
                            alignment: Alignment.bottomLeft,
                            child: FittedBox(
                                child: FixedOptionSelector<String>(
                              items: ["vmess"],
                              mutiple: false,
                              builder: (item) => Padding(
                                child: Text(
                                  item,
                                ),
                                padding: EdgeInsets.only(left: 2, right: 2),
                              ),
                              initialSelected: ["vmess"],
                              onSelectedChanged: (state) {
                                node.name = state.selected[0];
                              },
                            ))),
                      ),
                    ),
                  ]),
                  Flex(
                    direction: Axis.horizontal,
                    children: [
                      Expanded(
                          flex: 3,
                          child: TextFormField(
                            initialValue: node.config["remote_address"] ?? "",
                            decoration:
                                InputDecoration(labelText: "Remote Address"),
                            onSaved: (value) {
                              node.config['remote_address'] = value;
                            },
                          )),
                      SizedBox(
                        width: 16,
                      ),
                      Flexible(
                        child: Padding(
                            padding: EdgeInsets.only(top: 21),
                            child: NumberSelector<int>(
                              initialValue: node.config["remote_port"],
                              label: "Remote Port",
                              suggestions: [
                                new NumberSelectorSuggestion(443,
                                    "Default Port Number for HTTP over Sec. Layer"),
                                new NumberSelectorSuggestion(
                                    80, "Default Port Number for HTTP")
                              ],
                              onChanged: (value) {
                                node.config['remote_port'] = value;
                              },
                              valueCheck: (value) {
                                if (value <= 0) {
                                  return "Port must be greater than zero";
                                } else if (value > 65535) {
                                  return "Port must not be greater than 65535";
                                } else {
                                  return null;
                                }
                              },
                            )),
                      )
                    ],
                  ),
                  Flex(
                    direction: Axis.horizontal,
                    children: [
                      Expanded(
                          child: TextFormField(
                        initialValue: node.config['uuid'] ?? "",
                        decoration: InputDecoration(labelText: "UUID"),
                        onSaved: (value) {
                          node.config['uuid'] = value;
                        },
                      ))
                    ],
                  ),
                  Flex(
                    direction: Axis.horizontal,
                    children: [
                      Expanded(
                          child: Padding(
                              padding: EdgeInsets.only(top: 21),
                              child: NumberSelector<int>(
                                label: "Alter ID",
                                initialValue: node.config['alter_id'] ?? 0,
                                suggestions: [
                                  new NumberSelectorSuggestion(
                                      16, "default value of most server"),
                                  new NumberSelectorSuggestion(
                                      64, "recommended value")
                                ],
                                onChanged: (value) {
                                  node.config['alter_id'] = value;
                                },
                                valueCheck: (value) {
                                  if (value <= 0) {
                                    return "Alter ID must be greater than zero";
                                  } else {
                                    return null;
                                  }
                                },
                              ))),
                      SizedBox(
                        width: 16,
                      ),
                      Expanded(
                        child: TextFormSelector(
                          labelText: "Cipher",
                          initialValue: node.config['cipher'] ?? "",
                          suggestions: [
                            new TextFormSelectorSuggestion(
                                "auto", "Automatically Detected"),
                            new TextFormSelectorSuggestion(
                                "aes-128-gcm", "Modern AES crypto cipher"),
                            new TextFormSelectorSuggestion("chacha20-ploy1305",
                                "Modern chacha20 crypto cipher"),
                            new TextFormSelectorSuggestion(
                                "none", "Not recommended")
                          ],
                          onChanged: (value) {
                            node.config['cipher'] = value;
                          },
                        ),
                      )
                    ],
                  ),
                  Flex(
                    direction: Axis.horizontal,
                    children: [
                      Expanded(
                          child: ListTile(
                        contentPadding: EdgeInsets.only(top: 4, bottom: 4),
                        title: Text("Transport Sec. Layer"),
                        trailing: Switch(
                            value: node.config['tls'] ?? false,
                            onChanged: (value) {
                              node.config['tls'] = value;
                              requestRebuilld(); // Switch does not maintain any state, perform rebuild to get UI change
                            }),
                      )),
                      SizedBox(
                        width: 16,
                      ),
                      Expanded(
                        child: ListTile(
                          contentPadding: EdgeInsets.only(top: 4, bottom: 4),
                          title: Text("Skip Certificate Verify"),
                          trailing: Switch(
                            value: node.config['skip_cert_verify'] ?? false,
                            onChanged: (value) {
                              node.config['skip_cert_verify'] = value;
                              requestRebuilld();
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                  Flex(
                    direction: Axis.horizontal,
                    children: [
                      Expanded(
                          child: ListTile(
                        title: Text("Network (Addtional Transport Layer)"),
                        contentPadding: EdgeInsets.zero,
                        trailing: (() {
                          const ITEMS = const [
                            {'text': "None", 'item': "none"},
                            {'text': "WebSocket", 'item': "ws"},
                            {'text': "HTTP", 'item': "http"},
                          ];
                          return FittedBox(
                              child: FixedOptionSelector<Map<String, String>>(
                            items: ITEMS,
                            builder: (item) => Text(item['text']),
                            mutiple: false,
                            initialSelected: [
                              ITEMS.firstWhere((element) =>
                                  element['item'] ==
                                  (node.config['network'] ?? "none"))
                            ],
                            onSelectedChanged: (state) {
                              var selected = state.selected[0]['item'];
                              node.config['network'] =
                                  selected == "none" ? null : selected;
                              print(selected);
                              requestRebuilld();
                            },
                          ));
                        })(),
                      ))
                    ],
                  ),
                  getNetworkConfigEditor(context, node),
                  ButtonBar(
                    buttonPadding: EdgeInsets.all(4),
                    children: [
                      FlatButton(
                        child: Text("Apply"),
                        onPressed: () {},
                      )
                    ],
                  )
                ],
              ),
            ));
      },
    );
  }

  void requestRebuilld() {
    AsyncWidget.refresh(state: asyncKey.currentState);
  }

  Widget getNetworkConfigEditor(BuildContext context, Node node) {
    var network = node.config['network'] ?? "none";
    if (network == "none") {
      return Flex(direction: Axis.horizontal, children: [
        Text(
          "There are no more items if 'network' is 'None'",
          style: TextStyle(
              fontStyle: FontStyle.italic, color: Theme.of(context).hintColor),
        )
      ]);
    } else if (network == "ws") {
      return Column(
        children: [
          Flex(
            direction: Axis.horizontal,
            children: [
              Expanded(
                  child: TextFormField(
                decoration: InputDecoration(labelText: "Server Name"),
                initialValue: node.config['server_name'] ?? '',
                onSaved: (value) {
                  node.config['server_name'] = value.isEmpty ? null : value;
                },
              )),
              SizedBox(
                width: 16,
              ),
              Expanded(
                child: TextFormField(
                  decoration: InputDecoration(labelText: "Path"),
                  initialValue: node.config['ws_path'] ?? '',
                  onSaved: (value) {
                    node.config['ws_path'] = value.isEmpty ? null : value;
                  },
                ),
              )
            ],
          ),
          TextFormField(
            initialValue: json2yaml(
                node.config['ws_headers'] as Map<String, dynamic> ?? {}),
            decoration: InputDecoration(
                labelText: "Headers", hintText: "HeaderKey: HeaderValue"),
            onSaved: (value) {
              if (value.isEmpty) {
                node.config['ws_headers'] = null;
                return;
              }
              try {
                var map = loadYaml(value) as YamlMap;
                node.config['ws_headers'] = map.map<String, dynamic>(
                    (key, value) => new MapEntry(key, value));
              } on YamlException {}
            },
            validator: (value) {
              if (value.isEmpty) return null;
              try {
                if (!(loadYaml(value) is YamlMap))
                  return "Use syntax of YAML's map";
                return null;
              } catch (e) {
                return e.toString();
              }
            },
            maxLines: 4,
          ),
        ],
      );
    } else if (network == "http") {
      return Column(children: [
        Flex(
          direction: Axis.horizontal,
          children: [
            Expanded(
              child: TextFormSelector(
                initialValue: node.config['http_method'] ?? '',
                suggestions: [
                  new TextFormSelectorSuggestion("GET"),
                  new TextFormSelectorSuggestion("POST"),
                  new TextFormSelectorSuggestion("PUT"),
                ],
                labelText: "HTTP Method",
                onChanged: (value) {
                  node.config['http_method'] = value;
                },
              ),
            ),
            Expanded(
              flex: 3,
              child: TextFormField(
                initialValue:
                    ((node.config['http_paths'] ?? []) as List).join(';'),
                decoration: InputDecoration(
                    labelText: "HTTP Paths", hintText: "use ';' as separator"),
                onSaved: (value) {
                  node.config['http_paths'] = value.split(';');
                },
              ),
            ),
          ],
        ),
        TextFormField(
          initialValue:
              json2yaml(node.config['http_headers'] as Map<String, List> ?? {}),
          decoration: InputDecoration(
              labelText: "Headers",
              hintText:
                  "HeaderKey: \n  - 'HeaderValue1'\n  - 'HeaderValue2'\nHeaderKey2:\n  - 'HeaderValue'"),
          onSaved: (value) {
            if (value.isEmpty) {
              node.config['http_headers'] = null;
              return;
            }
            try {
              var map = loadYaml(value) as YamlMap;
              node.config['http_headers'] = map
                  .map<String, List>((key, value) => new MapEntry(key, value));
            } on YamlException {}
          },
          validator: (value) {
            if (value.isEmpty) return null;
            try {
              if (!(loadYaml(value) is YamlMap))
                return "Use syntax of YAML's map";
              return null;
            } catch (e) {
              return e.toString();
            }
          },
          maxLines: 5,
        )
      ]);
    } else {
      return Padding(
          padding: EdgeInsets.all(16),
          child: Flex(
            direction: Axis.vertical,
            children: [
              Text(
                  "This editor '$runtimeType' could not edit the config for network '$network'."),
              Text(
                  "Something wrong may have happened, or just something unsupported yet."),
              Text("If this message keeps always, please contract maintainer.")
            ],
          ));
    }
  }
}

class WithLabel extends StatelessWidget {
  final EdgeInsetsGeometry padding;
  final Widget child;
  final String labelText;
  WithLabel(
      {Key key, this.padding = EdgeInsets.zero, this.child, this.labelText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomRight,
      padding: padding,
      child: Column(
        children: [
          Align(
            child: Text(
              labelText,
              textScaleFactor: 0.85,
              style: TextStyle(color: Theme.of(context).hintColor),
            ),
            alignment: Alignment.bottomLeft,
          ),
          child,
        ],
      ),
    );
  }
}
