import 'package:clearway/clash/config.dart';
import 'package:clearway/common.dart';
import 'package:clearway/components/async_widget.dart';
import 'package:clearway/components/fixed_option_selector.dart';
import 'package:clearway/conftool.dart';
import 'package:clearway/global.dart';
import 'package:clearway/page_nodes/subs_list.dart';
import 'package:clearway/page_settings/tiles.dart';
import 'package:clearway/static_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class NewSubscriptionDialog extends StatefulWidget {
  NewSubscriptionDialog({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return NewSubscriptionDialogState();
  }
}

class NewSubscriptionDialogState extends State<NewSubscriptionDialog> {
  NewSubscriptionTicket ticket = new NewSubscriptionTicket(type: 'clash');

  @override
  Widget build(BuildContext context) {
    var urlTextController = new TextEditingController();
    var body = <Widget>[
      Row(children: [
        Expanded(
            child: Text("Type" + (DEBUG ? "(current: ${ticket.type})" : ""))),
        FixedOptionSelector<String>(
          items: ['folder', 'clash'],
          initialSelected: [ticket.type],
          mutiple: false,
          builder: (item) => Text(item),
          onSelectedChanged: (state) => setState(() {
            ticket.type = state.selected[0];
          }),
        ),
      ]),
    ];
    if (ticket.type == "folder") {
      body.add(TextField(
        key: UniqueKey(),
        decoration: InputDecoration(labelText: "Folder Name"),
        onChanged: (value) => ticket.name = value,
      ));
    } else if (ticket.type == "clash") {
      body.add(TextField(
        key: UniqueKey(),
        decoration: InputDecoration(labelText: "Subscription Name"),
        onChanged: (value) => ticket.name = value,
      ));
      body.add(TextField(
        key: UniqueKey(),
        decoration: InputDecoration(labelText: "Configuration URI"),
        controller: urlTextController,
        onChanged: (value) => ticket.url = value,
      ));
    }
    return AlertDialog(
      title: Text("New Subscription"),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: body,
      ),
      actionsPadding: EdgeInsets.all(4),
      buttonPadding: EdgeInsets.only(left: 12, right: 12),
      actions: [
        FlatButton(
          child: Text("PASTE URI"),
          onPressed: ticket.type == "folder"
              ? null
              : () {
                  Clipboard.getData("text/plain").then((value) {
                    if (value != null) {
                      urlTextController.text = value.text;
                      ticket.url = value.text;
                    }
                  });
                },
        ),
        FlatButton(
          child: Text(getOKButtonText()),
          onPressed: () {
            Navigator.of(context).pop(ticket);
          },
        ),
      ],
    );
  }

  String getOKButtonText() {
    if (ticket.type == 'folder') {
      return "CREATE";
    } else if (ticket.type == 'clash') {
      return "GET & CREATE";
    } else {
      return "OK";
    }
  }
}

class NewSubscriptionTicket {
  String type;
  String url;
  String name;
  NewSubscriptionTicket({this.type, this.url, this.name});

  void clear() {
    type = null;
    url = null;
    name = null;
  }
}

class AddSubscriptionIconButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.add_box),
      tooltip: "Add subscription...",
      onPressed: () {
        showDialog<NewSubscriptionTicket>(
            context: context,
            builder: (context) => NewSubscriptionDialog()).then((ticket) {
          if (ticket == null || ticket.type == null) {
            return;
          }
          var conf = ProxyNodesConfHolder.getSync();
          if (ticket.type == "folder") {
            var newSubs = new NodeSubscription(ticket.name, null, []);
            conf.update(newSubs);
            conf.save().then((value) => showTextSnackbar(
                context,
                "\"${ticket.name}\" has been created.",
                SnackBarAction(
                  label: "RELOAD LIST",
                  onPressed: () => ProxyNodesConfHolder.get()
                      .then((holder) => holder.load())
                      .then((_) => AsyncWidget.refresh()),
                )));
          } else if (ticket.type == "clash") {
            var sourceUrl = "x-type-clash://" + ticket.url;
            var source = new ClashProxiesSource(sourceUrl);
            var pContext = context;
            showDialog(
                context: context,
                builder: (context) {
                  return AsyncWidget(
                    placeholder: SimpleDialog(
                      contentPadding: EdgeInsets.all(24),
                      children: [
                        Row(children: [
                          CircularProgressIndicator(),
                          Padding(
                              padding: EdgeInsets.all(16),
                              child: Text("Downloading", textScaleFactor: 1.1))
                        ])
                      ],
                    ),
                    computation: () async {
                      List<Node> nodes;
                      try {
                        nodes = await source.getNodesFromRemote();
                      } on ConfigFormatException catch (e) {
                        Navigator.of(context).pop();
                        showTextSnackbar(
                            pContext,
                            e.shortMessage,
                            SnackBarAction(
                              label: "DETAILS",
                              onPressed: () => showExplainableExceptionDialog<
                                      ConfigFormatException>(
                                  context: pContext,
                                  e: e,
                                  titleLabel: e.shortMessage,
                                  childrenBuilder: (e) => [
                                        IconButton(
                                          icon: Icon(Icons.content_copy),
                                          tooltip:
                                              "Copy Downloaded Configuration",
                                          onPressed: () => Clipboard.setData(
                                              new ClipboardData(
                                                  text: e.configContent)),
                                        )
                                      ]),
                            ));
                      } on ExplainableException catch (e) {
                        Navigator.of(context).pop();
                        showTextSnackbar(
                            pContext,
                            e.shortMessage,
                            SnackBarAction(
                              label: "DETAILS",
                              onPressed: () => showExplainableExceptionDialog(
                                context: pContext,
                                e: e,
                              ),
                            ));
                      }
                      if (nodes != null) {
                        var subs =
                            new NodeSubscription(ticket.name, sourceUrl, nodes);
                        conf.update(subs);
                        await conf.save();
                        showTextSnackbar(
                            pContext,
                            "\"${ticket.name}\" has been created.",
                            SnackBarAction(
                              label: "RELOAD LIST",
                              onPressed: () => ProxyNodesConfHolder.get()
                                  .then((holder) => holder.load())
                                  .then((_) => AsyncWidget.refresh()),
                            ));
                        Navigator.of(context).pop();
                      }

                      return Placeholder();
                    },
                  );
                });
          }
        });
      },
    );
  }
}

void showExplainableExceptionDialog<T extends ExplainableException>(
    {BuildContext context,
    T e,
    SingleValueTransformer<T, List<Widget>> childrenBuilder,
    String titleLabel}) {
  var content = <Widget>[
    Text(e.message),
    SizedBox(
      height: 8,
    ),
  ];
  if (childrenBuilder != null) {
    content.addAll(childrenBuilder(e));
  }
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(titleLabel ?? "Unhandled Exception"),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(2))),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: content,
          ),
          actions: [
            FlatButton(
              child: Text("OK"),
              onPressed: () => Navigator.of(context).pop(),
            )
          ],
        );
      });
}
