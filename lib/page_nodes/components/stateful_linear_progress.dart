import 'package:flutter/material.dart';

class StatefulLinearProgressIndicator extends StatefulWidget {
  StatefulLinearProgressIndicator({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new StatefulLinearProgressIndicatorState();
  }
}

class StatefulLinearProgressIndicatorState
    extends State<StatefulLinearProgressIndicator> {
  double value = 0;

  setValue(double v) {
    setState(() {
      value = v;
    });
  }

  @override
  Widget build(BuildContext context) {
    return LinearProgressIndicator(
      value: value,
    );
  }
}
