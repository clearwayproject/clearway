/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:clearway/common.dart';
import 'package:flutter/material.dart';

class TextFormSelectorSuggestion {
  final String text;
  final String tip;
  TextFormSelectorSuggestion(this.text, [this.tip]);

  String getSuggestionString() {
    if (tip != null) {
      return "$text ($tip)";
    } else {
      return text;
    }
  }
}

class TextFormSelector extends StatelessWidget {
  final String initialValue;
  final List<TextFormSelectorSuggestion> suggestions;
  final String labelText;
  final SingleValueRecvicer<String> onChanged;
  final TextEditingController controller = new TextEditingController();
  TextFormSelector(
      {this.initialValue, this.suggestions, this.labelText, this.onChanged}) {
    controller.text = initialValue;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
            flex: 3,
            child: TextFormField(
              controller: controller,
              decoration: InputDecoration(labelText: labelText),
            )),
        Flexible(
            child: Padding(
                padding: EdgeInsets.only(top: 22),
                child: PopupMenuButton<TextFormSelectorSuggestion>(
                  child: Icon(Icons.arrow_drop_down),
                  itemBuilder: (context) => suggestions
                      .map((e) => new PopupMenuItem(
                            child: Text(e.getSuggestionString()),
                            value: e,
                          ))
                      .toList(),
                  initialValue: suggestions.firstWhere(
                      (element) => element.text == controller.text,
                      orElse: () => null),
                  onSelected: (value) => controller.text = value.text,
                  tooltip: "Show suggestions",
                )))
      ],
    );
  }
}
