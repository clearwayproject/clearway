/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:clearway/clash/controller.dart';
import 'package:clearway/clash/status_protocol.dart';
import 'package:clearway/common.dart';
import 'package:logging/logging.dart';

import 'clash_process.dart';

class ClashProcessManager implements StatusProtocol {
  final _logger = new Logger("ClashProcessManager");
  String binaryPath;
  String spawnMethod = "dartio_platform";
  ClashProcess _process;

  void renewProcess([String type]) {
    if (type == "dartio_platform") {
      _process = new PlatformClashProcess(binaryPath);
      _logger.info("renew process to $_process");
    } else {
      throw new UnimplementedError();
    }
  }

  bool canUse() => (binaryPath != null);
  bool canUseErrorBuffer() => _process is ErrorBufferProtocol;
  bool canUsePID() => _process is PIDProtocol;

  ClashController get controller => _process?.controller;

  Future<WorkingStatus> start() => _process?.start();
  Future<WorkingStatus> stop() => _process?.stop();
  Future<WorkingStatus> restart() => _process?.restart();

  Stream<String> get stderr => _process?.stderr;
  Stream<String> get stdout => _process?.stdout;

  Stream<WorkingStatus> get statusEvents => _process?.statusEvents;

  WorkingStatus get currentStatus =>
      _process?.currentStatus ?? WorkingStatus.NOT_WORKING;

  StringBuffer get errorBuffer =>
      (_process as ErrorBufferProtocol)?.errorBuffer;
  String get errorMessage => errorBuffer?.toString();

  int get pid => (_process as PIDProtocol).pid;

  ClashProcess get process {
    return _process;
  }

  @override
  Future<WorkingStatus> changeStatus(WorkingStatus status) {
    return _process?.changeStatus(status);
  }
}
