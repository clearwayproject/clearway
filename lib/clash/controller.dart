/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'dart:async';
import 'dart:convert' show json;
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';

class RemoteClashException implements Exception {
  final String type;
  final String shortMessage;
  final String message;
  final String remoteMessage;
  RemoteClashException(
      this.type, this.shortMessage, this.message, this.remoteMessage);
}

class ClashController {
  final _logger = new Logger("ClashController");
  Uri remoteAddress;
  ClashController(this.remoteAddress);

  Stream<TrafficDataChunk> traffic() {
    var client = new http.Client();
    var controller = new StreamController<TrafficDataChunk>.broadcast(
        onCancel: () => client.close());
    client
        .send(new http.StreamedRequest(
            "GET", remoteAddress.replace(pathSegments: ['traffic'])))
        .then((value) => value.stream.toStringStream())
        .then((value) {
      var buffer = new StringBuffer();
      return value.transform(new StreamTransformer<String, String>.fromHandlers(
        handleData: (data, sink) {
          buffer.write(data);
          if (data == '}') {
            sink.add(buffer.toString());
            buffer.clear();
          }
        },
        handleError: (error, stackTrace, sink) {
          sink.addError(error, stackTrace);
          buffer.clear();
        },
        handleDone: (sink) {
          buffer.clear();
          sink.close();
        },
      ));
    }).then((value) {
      value.listen((event) {
        try {
          var jsonObj = json.decode(event) as Map<String, int>;
          if (TrafficDataChunk.checkMap(jsonObj)) {
            controller.add(TrafficDataChunk.fromMap(jsonObj));
          }
        } on FormatException {
          // ingore vaild format error
        } on TypeError {
          // ignore type error when casting
        }
      });
    });
    return controller.stream;
  }

  Stream<RemoteLogChunk> logs() {
    var client = new http.Client();
    var controller = new StreamController<RemoteLogChunk>.broadcast(
        onCancel: () => client.close());
    client
        .send(new http.StreamedRequest(
            "GET", remoteAddress.replace(pathSegments: ['logs'])))
        .then((value) => value.stream.toStringStream())
        .then((value) {
      var buffer = new StringBuffer();
      return value.transform(new StreamTransformer<String, String>.fromHandlers(
        handleData: (data, sink) {
          buffer.write(data);
          if (data == '}') {
            sink.add(buffer.toString());
            buffer.clear();
          }
        },
        handleError: (error, stackTrace, sink) {
          sink.addError(error, stackTrace);
          buffer.clear();
        },
        handleDone: (sink) {
          buffer.clear();
          sink.close();
        },
      ));
    }).then((value) {
      value.listen((event) {
        try {
          var jsonObj = json.decode(event) as Map<String, String>;
          if (RemoteLogChunk.checkMap(jsonObj)) {
            controller.add(RemoteLogChunk.fromMap(jsonObj));
          }
        } on FormatException {
          // ingore vaild format error
        } on TypeError {
          // ignore type error when casting
        }
      });
    });
    return controller.stream;
  }

  Future<bool> applyConfig(String path, {bool force: true}) async {
    var response = await http.put(
        remoteAddress.replace(
            pathSegments: ['configs'],
            queryParameters: {'force': force.toString()}),
        body: json.encode({'path': path}));
    if (response.statusCode == 204) {
      return true;
    } else {
      _logger.shout(() =>
          "error happened. request to: ${response.request.url}, reponded with ${response.statusCode} and ${response.body}");
      throw RemoteClashException(
          "apply_config_failed",
          "Could not apply your config",
          "Remote clash process responded with fault (HTTP Status is ${response.statusCode} ${response.reasonPhrase})",
          response.body);
    }
  }

  Future<bool> selectProxy(String selectorName, String proxyName) async {
    var response = await http.put(
        remoteAddress.replace(pathSegments: ['proxies', selectorName]),
        body: json.encode({'name': proxyName}));
    if (response.statusCode == 204) {
      return true;
    } else {
      throw RemoteClashException(
          "select_proxy_failed",
          "Could not select your proxy \"$proxyName\"",
          "Remote clash process responded with fault (HTTP Status is ${response.statusCode} ${response.reasonPhrase})",
          response.body);
    }
  }

  String toString() => "$runtimeType(remoteAddress=$remoteAddress)";
}

class RemoteLogChunk {
  final String type; // types: info/warning/error/debug
  final String payload;

  const RemoteLogChunk(this.type, this.payload);

  static bool checkMap(Map map) {
    return (map.containsKey('type') && map.containsKey('payload')) &&
        (map['type'] is String && map['payload'] is String);
  }

  factory RemoteLogChunk.fromMap(Map<String, String> map) {
    return new RemoteLogChunk(map['type'], map['payload']);
  }
}

class TrafficDataChunk {
  final int up; // byte(s)
  final int down; // byte(s)

  const TrafficDataChunk(this.up, this.down);

  static bool checkMap(Map map) {
    return (map.containsKey('up') && map.containsKey('down')) &&
        (map['up'] is int && map['down'] is int);
  }

  factory TrafficDataChunk.fromMap(Map<String, int> map) {
    return new TrafficDataChunk(map['up'], map['down']);
  }
}
