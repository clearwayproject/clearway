/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:clearway/common.dart';
import 'package:clearway/conftool.dart';
import 'package:deep_pick/deep_pick.dart';
import 'package:json2yaml/json2yaml.dart';
import 'package:http/http.dart' as http;
import 'package:yaml/yaml.dart';

class ClashConfigGen {
  Map<String, dynamic> config = {};

  String get result => json2yaml(config);

  ClashConfigGen() {
    setIPv6(true);
    setLogLevel('info');
    setOutboundInterfaceName('en0');
  }

  void setLocalProxyPort(int http, int socks, int redir) {
    config.addAll({
      'port': http,
      'socks-port': socks,
      'redir-port': redir,
    });
  }

  void setMode(String mode) {
    if (['rule', 'global', 'direct'].contains(mode)) {
      config['mode'] = mode;
    }
  }

  void addProxy(ClashProxyConfGen proxy) {
    if (!config.containsKey('proxies')) {
      config['proxies'] = <Map<String, dynamic>>[];
    }
    (config['proxies'] as List<Map<String, dynamic>>).add(proxy.gen());
  }

  void addProxyGroup(ClashProxyGroupGen group) {
    if (!config.containsKey('Proxy Group')) {
      config['Proxy Group'] = <Map<String, dynamic>>[];
    }
    (config['Proxy Group'] as List<Map<String, dynamic>>).add(group.gen());
  }

  void setLogLevel(String level) {
    // level: info, warning, error, debug, slient
    assert(['info', 'warning', 'error', 'debug', 'slient'].contains(level));
    config['log-level'] = level;
  }

  void setIPv6(bool allow) {
    config['ipv6'] = allow;
  }

  void setOutboundInterfaceName(String name) {
    config['interface-name'] = name;
  }

  void addNameserver(String host) {
    if (!config.containsKey('nameserver')) {
      config['nameserver'] = <String>[];
    }
    (config['nameserver'] as List<String>).add(host);
  }
}

abstract class ClashProxyConfGen {
  Map<String, dynamic> gen();
}

class ClashVmessConfGen implements ClashProxyConfGen {
  final String name;
  final String remoteAddress;
  final int remotePort;
  final String uuid;
  final int alterId;
  final String cipher;
  final bool udp;
  final bool tls;
  final bool skipCertVerify;
  final String network;
  final String serverName;
  final String wsPath;
  final Map<String, dynamic> wsHeaders;
  final String httpMethod;
  final List<String> httpPaths;
  final Map<String, List<dynamic>> httpHeaders;
  ClashVmessConfGen(
      {this.name,
      this.remoteAddress,
      this.remotePort,
      this.uuid,
      this.alterId,
      this.cipher,
      this.udp = false,
      this.tls = false,
      this.skipCertVerify = false,
      this.network,
      this.serverName,
      this.wsPath,
      this.wsHeaders,
      this.httpMethod,
      this.httpPaths,
      this.httpHeaders});

  @override
  Map<String, dynamic> gen() {
    var base = {
      'name': name,
      'type': 'vmess',
      'server': remoteAddress,
      'port': remotePort,
      'uuid': uuid,
      'alterId': alterId,
      'cipher': cipher,
      'skip-cert-verify': skipCertVerify,
      'udp': udp,
      'tls': tls,
    };
    if (network == 'ws') {
      base.addAll({
        'network': network,
        'ws-path': wsPath,
        'ws-headers': wsHeaders,
        'servername': serverName
      });
    } else if (network == 'http') {
      base.addAll({
        'network': network,
        'http-opts': {
          'method': httpMethod,
          'path': httpPaths,
          'headers': httpHeaders
        }
      });
    }
    return base;
  }

  Node toNode() => new Node(name, 'vmess', {
        'remote_address': remoteAddress,
        'remote_port': remotePort,
        'uuid': uuid,
        'alter_id': alterId,
        'cipher': cipher,
        'udp': udp,
        'tls': tls,
        'skip_cert_verify': skipCertVerify,
        'network': network,
        'server_name': serverName,
        'ws_path': wsPath,
        'ws_headers': wsHeaders,
        'http_method': httpMethod,
        'http_paths': httpPaths,
        'http_headers': httpHeaders,
      });

  static int processStringInt(dynamic s) {
    if (s is String) {
      return int.parse(s);
    } else if (s is int) {
      return s;
    } else {
      throw new FormatException(
          "$s is neither a lexical int String nor a int", s);
    }
  }

  /// Parse a clash config [Map].
  /// Throw [FormatException] when `port` or `alterId` is neither [int] nor lexical int [String],
  /// [TypeError] when any field cloud not be casted correctly
  static ClashVmessConfGen parse(Map<String, dynamic> mapping) {
    return new ClashVmessConfGen(
      name: mapping['name'],
      remoteAddress: mapping['server'],
      remotePort: processStringInt(mapping['port']),
      uuid: mapping['uuid'],
      alterId: processStringInt(mapping['alterId']),
      cipher: mapping['cipher'],
      skipCertVerify: mapping['skip-cert-verify'],
      udp: mapping['udp'],
      tls: mapping['tls'],
      network: mapping['network'],
      serverName: mapping['servername'],
      wsPath: mapping['ws-path'],
      wsHeaders: mapping['ws-headers'],
      httpHeaders: pick(mapping, 'http-opts', 'headers')
          .asMapOrNull<String, List<dynamic>>(),
      httpMethod: pick(mapping, 'http-opts', 'method').asStringOrNull(),
      httpPaths: pick(mapping, 'http-opts', 'path').asListOrNull<String>(),
    );
  }
}

abstract class ClashProxyGroupGen {
  Map<String, dynamic> gen();
}

class SelectProxyGroupGen implements ClashProxyGroupGen {
  String name;
  List<String> proxyNames = [];
  SelectProxyGroupGen({this.name, List<String> proxyNames = const []}) {
    this.proxyNames.addAll(proxyNames);
  }

  void addName(String name) {
    if (name != null) proxyNames.add(name);
  }

  void addProxyConfGen(ClashProxyConfGen gen) {
    addName(gen.gen()['name']);
  }

  @override
  Map<String, dynamic> gen() {
    return {'name': name, 'type': 'select', 'proxies': proxyNames};
  }
}

class ClashProxiesSource {
  final String sourceUrl;
  String sourceType;
  Uri realUri;

  ClashProxiesSource(this.sourceUrl) {
    assert(sourceUrl != null);
    _parseFromString(sourceUrl);
  }

  /// parse a source url and set fields
  /// A source url like standard URI but have two protocol parts:
  /// `x-type-<config_type>://<real_uri>`, `real_uri` accepts like standard URI for example `https://example.com/config.yaml`.
  ///   Throw [FormatException] if `real_uri` is not a URI, throw
  _parseFromString(String url) {
    final typeRegExp = new RegExp("x-type-([A-Za-z0-9]*)://");
    if (sourceUrl.startsWith(typeRegExp)) {
      var typeMatch = typeRegExp.firstMatch(sourceUrl);
      sourceType = typeMatch.group(1);
      if (sourceType == null) {
        throw new SourceURLParseException(
            'source_type_is_null',
            "Could not found source type",
            "Could not found source type from $sourceUrl",
            sourceUrl);
      }
      realUri = Uri.parse(sourceUrl.substring(typeMatch.end));
    }
  }

  /// Get nodes from remote config, using HTTP GET.
  /// Throw [UnimplementedError] when [sourceType] could not be accepted.
  /// When [sourceType] == "clash", it may throw [ConfigFormatException] when remote config's type could not be accepted.
  Future<List<Node>> getNodesFromRemote() async {
    var response = await http.get(realUri);
    if (sourceType == "clash") {
      return parseClashConfig(response.body);
    } else {
      throw new UnimplementedError(
          "getNodesFromRemote() does not implemented for $sourceType");
    }
  }

  List<Node> parseClashConfig(String content) {
    var yaml = loadYaml(content);
    if (!(yaml is YamlMap)) {
      throw new ConfigFormatException(
          'config-base-format', 'YAML Map', content);
    }
    var proxies = yaml['proxies'];
    if (proxies == null || !(proxies is YamlList)) {
      throw new ConfigFormatException(
          'config-format',
          "Clash Config with Proxies",
          content,
          "It doesn't have correct value of \"proxyies\"");
    }
    var items = <Node>[];
    for (YamlMap item in proxies) {
      var casted =
          item.map<String, dynamic>((key, value) => new MapEntry(key, value));
      if (item['type'] == 'vmess') {
        items.add(ClashVmessConfGen.parse(casted).toNode());
      }
    }
    return items;
  }
}

class SourceURLParseException implements ExplainableException {
  final String type;
  final String shortMessage;
  final String message;
  final String sourceUrl;
  SourceURLParseException(
      this.type, this.shortMessage, this.message, this.sourceUrl);

  @override
  String toString() {
    return "$runtimeType($type, $shortMessage)";
  }
}

class ConfigFormatException implements ExplainableException {
  final String type;
  final String exceptConfigFormat;
  final String configContent;
  final String extraReason;
  final String shortMessage = "Could not parse remote config: wrong format";
  String get message =>
      "Could not parse remote config because of wrong format, it is expected to be $exceptConfigFormat, but it didn't is. $extraReason";

  ConfigFormatException(this.type, this.exceptConfigFormat, this.configContent,
      [this.extraReason = '']);
}
