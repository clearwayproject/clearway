/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:clearway/common.dart';

abstract class StatusProtocol {
  WorkingStatus get currentStatus;
  Stream<WorkingStatus> get statusEvents;
  Future<WorkingStatus> start();
  Future<WorkingStatus> stop();
  Future<WorkingStatus> restart();
  Future<WorkingStatus> changeStatus(WorkingStatus status) {
    if (currentStatus == status) return new Future.value(currentStatus);
    if (status == WorkingStatus.NOT_WORKING) {
      return stop();
    } else if (status == WorkingStatus.WORKING) {
      return start();
    } else {
      throw new UnimplementedError(
          "changeStatus(WorkingStatus) does not support targeting $status");
    }
  }
}
