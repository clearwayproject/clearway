/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:clearway/clash/controller.dart';
import 'package:clearway/common.dart';
import 'package:logging/logging.dart';

import 'status_protocol.dart';

abstract class ClashProcess extends StatusProtocol {
  ClashController get controller;
  String get controlAddress;
  Stream<String> get stdout;
  Stream<String> get stderr;
}

abstract class PIDProtocol {
  int get pid;
}

abstract class ErrorBufferProtocol {
  StringBuffer get errorBuffer;
  String get errorMessage;
}

class PlatformClashProcess
    implements ClashProcess, PIDProtocol, ErrorBufferProtocol {
  final Logger _logger = new Logger("PlatformClashProcess");
  final Logger _clashLogger = new Logger("Clash");

  StreamController<WorkingStatus> statusEventController =
      new StreamController.broadcast();

  final String binaryPath;
  Process process;

  PlatformClashProcess(this.binaryPath);

  static Future<int> getFreePortNumber() async {
    var ss = await ServerSocket.bind("127.0.0.1", 0);
    var port = ss.port;
    await ss.close();
    return port;
  }

  @override
  int pid;

  @override
  Stream<String> stderr;

  StreamSubscription stderrSubscription;

  @override
  Stream<String> stdout;

  StreamSubscription stdoutSubscription;

  @override
  String controlAddress;

  @override
  ClashController controller;

  WorkingStatus _currentStatus = WorkingStatus.NOT_WORKING;
  @override
  WorkingStatus get currentStatus => _currentStatus;
  set currentStatus(WorkingStatus s) {
    _currentStatus = s;
    statusEventController.add(s);
  }

  @override
  Future<WorkingStatus> restart() async {
    if (currentStatus != WorkingStatus.WORKING) {
      await stop();
    }
    await start();
    return currentStatus;
  }

  @override
  Future<WorkingStatus> start() async {
    var portN = await getFreePortNumber();
    var ctlAddress = "127.0.0.1:$portN";
    process = await Process.start(binaryPath, ['-ext-ctl', ctlAddress]);
    pid = process.pid;
    _logger.info("process started, pid=$pid");
    stdout = stdTextTransform(process.stdout);
    stderr = stdTextTransform(process.stderr);
    controlAddress = ctlAddress;
    controller = new ClashController(new Uri.http(ctlAddress, ''));
    currentStatus = WorkingStatus.WORKING;
    doListenSTDIOAndLog();
    return currentStatus;
  }

  doListenSTDIOAndLog() {
    stdoutSubscription = stdout.listen((event) => _clashLogger.info(event));
    stderrSubscription = stderr.listen((event) {
      _clashLogger.info(event);
      errorBuffer.writeln(event);
    });
  }

  static stdTextTransform(Stream<List<int>> s) {
    var buffer = new StringBuffer();
    return s.transform(utf8.decoder).transform(
        new StreamTransformer<String, String>.fromHandlers(
            handleData: (s, sink) {
      if (s == '\n') {
        sink.add(buffer.toString());
        buffer.clear();
      } else {
        buffer.write(s);
      }
    })).asBroadcastStream();
  }

  @override
  Stream<WorkingStatus> get statusEvents => statusEventController.stream;

  void clear() {
    process = null;
    pid = null;
    stderr = null;
    stdout = null;
    currentStatus = WorkingStatus.NOT_WORKING;
    controlAddress = null;
    controller = null;
    stdoutSubscription.cancel();
    stdoutSubscription = null;
    stderrSubscription.cancel();
    stderrSubscription = null;
    errorBuffer.clear();
  }

  @override
  Future<WorkingStatus> stop() async {
    process.kill();
    await process.exitCode;
    clear();
    return currentStatus;
  }

  @override
  Future<WorkingStatus> changeStatus(WorkingStatus status) {
    if (currentStatus == status) return new Future.value(currentStatus);
    if (status == WorkingStatus.NOT_WORKING) {
      return stop();
    } else if (status == WorkingStatus.WORKING) {
      return start();
    } else {
      throw new UnimplementedError(
          "changeStatus(WorkingStatus) does not support targeting $status");
    }
  }

  String toString() => "$runtimeType(currentStatus=$currentStatus, pid=$pid)";

  @override
  StringBuffer errorBuffer = new StringBuffer();

  @override
  String get errorMessage => errorBuffer.toString();
}
