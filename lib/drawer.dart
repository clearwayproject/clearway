/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'dart:async';
import 'package:clearway/common.dart';
import 'package:clearway/static_config.dart';
import 'package:flutter/material.dart';

class DrawerHolder {
  static Widget _drawer;
  static StreamController eventpush =
      new StreamController<Map<String, dynamic>>.broadcast();

  DrawerHolder._init();

  static ClearWayDrawer createDrawer() => new ClearWayDrawer(eventpush.stream);

  static ClearWayDrawer get global {
    if (_drawer == null) {
      _drawer = createDrawer();
    }
    return _drawer;
  }

  static setStatus(WorkingStatus st) {
    eventpush.add({'type': 'status', 'value': st});
  }

  static setSelected(String routeName) {
    eventpush.add({'type': 'selected', 'value': routeName});
  }
}

class DrawerNavigatorObserver extends NavigatorObserver {
  @override
  void didPush(Route route, Route previousRoute) {
    DrawerHolder.setSelected(route.settings.name);
  }

  @override
  void didPop(Route route, Route previousRoute) {
    DrawerHolder.setSelected(route.settings.name);
  }
}

class ClearWayDrawer extends StatefulWidget {
  final Stream<Map<String, dynamic>> events;
  ClearWayDrawer(this.events);

  _ClearWayDrawerState createState() => new _ClearWayDrawerState(events);
}

class _ClearWayDrawerState extends State<ClearWayDrawer> {
  WorkingStatus status = WorkingStatus.NOT_WORKING;
  static String selectedRouteName;

  final Stream<Map<String, dynamic>> events;
  StreamSubscription eventSubscription;

  _ClearWayDrawerState(this.events);

  @override
  void initState() {
    super.initState();
    eventSubscription = events.listen(_whenEventComes);
  }

  @override
  void dispose() {
    super.dispose();
    eventSubscription.cancel();
  }

  void _whenEventComes(Map<String, dynamic> e) {
    if (e['type'] == 'status') {
      setState(() {
        var value = e['value'] ?? WorkingStatus.NOT_WORKING;
        status = value;
      });
    } else if (e['type'] == 'selected') {
      setState(() {
        selectedRouteName = e['value'] as String;
      });
    }
  }

  bool isSelected(String name) {
    return selectedRouteName == name;
  }

  @override
  Widget build(BuildContext context) {
    var widgets = <Widget>[
      ListTile(
        leading: Icon(getStatusIcon()),
        title: Text("Status"),
        onTap: () => Navigator.of(context).popAndPushNamed('/status'),
        selected: isSelected('/status'),
      ),
      ListTile(
        leading: Icon(Icons.device_hub),
        title: Text("Nodes"),
        onTap: () => Navigator.of(context).popAndPushNamed('/nodes'),
        selected: isSelected('/nodes'),
      ),
      ListTile(leading: Icon(Icons.call_split), title: Text("Router")),
      ListTile(leading: Icon(Icons.pan_tool), title: Text("Filter")),
      ListTile(
        leading: Icon(Icons.settings),
        title: Text("Settings"),
        onTap: () => Navigator.of(context).popAndPushNamed('/settings'),
        selected: isSelected('/settings'),
      )
    ];
    if (DEBUG) {
      widgets.add(ListTile(
        leading: Icon(Icons.developer_mode),
        title: Text("Development"),
        onTap: () => Navigator.of(context).popAndPushNamed('/development'),
        selected: isSelected('/development'),
      ));
    }
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: widgets,
      ),
    );
  }

  IconData getStatusIcon() {
    switch (status) {
      case WorkingStatus.NOT_WORKING:
        return Icons.face;
      case WorkingStatus.CONNECTING:
        return Icons.flight_takeoff;
      case WorkingStatus.WORKING:
        return Icons.mood;
      case WorkingStatus.ERROR:
        return Icons.error;
      default:
        return null;
    }
  }
}
