/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'dart:io';

import 'package:clearway/clash/config.dart';
import 'package:logging/logging.dart';

import 'common.dart';
import 'package:yaml/yaml.dart';
import 'package:json2yaml/json2yaml.dart';
import 'package:path/path.dart' as _p;

// Configuration Folder Structure
// - subs
//   - sub1
//    - _source.txt
//    - node1.yaml
//   - others
//    - node2.yaml
// - subs.yaml

class ProxyNodesConf {
  final Logger _logger = new Logger('ProxyNodesConf');
  static const String OTHERSK = 'others';
  String rootPath;
  Map<String, NodeSubscription> _subscriptions = {};
  Map<String, NodeSubscription> get subscriptions {
    var m = <String, NodeSubscription>{};
    m.addAll(_subscriptions);
    m.addAll(_changes);
    m.removeWhere((key, value) => key == null || value == null);
    _logger.fine(() =>
        "counted ${m.length} subscription(s), ${_changes.length} changes");
    return new Map.unmodifiable(m);
  }

  set subscriptions(Map<String, NodeSubscription> m) => _subscriptions = m;

  Map<String, NodeSubscription> _changes = {};

  bool get hasChanges => _changes.length > 0;

  ProxyNodesConf(this.rootPath);

  void update(NodeSubscription sub) {
    _changes[sub.name] = sub;
    _logger.fine(() => "change added, total ${_changes.length} changes");
  }

  static Future<ProxyNodesConf> fromSettings() {
    return ClearWaySetings.get(ClearWaySetings.CONFIGURATION_FOLDER_PATH)
        .then((value) => new ProxyNodesConf(value));
  }

  String get subsConfPath => _p.join(rootPath, 'subs.yaml');

  Future load() async {
    var subsFile = new File(subsConfPath);
    if (!(await subsFile.exists())) {
      await subsFile.writeAsString(json2yaml({'version': 1, 'names': []}),
          flush: true);
    }
    var subsConf =
        (loadYaml(await subsFile.readAsString(), sourceUrl: subsConfPath)
            as YamlMap);
    var subsVersion = subsConf['version'] as int;
    switch (subsVersion) {
      case 1:
        // current version, nothing to do
        break;
      default:
        throw new UnsupportedError(
            "unsupported internal type in $subsConfPath");
    }
    _subscriptions.clear();
    var subsNames = (subsConf['names'] as YamlList) ?? [];
    for (var name in subsNames) {
      var dir = new Directory(_p.join(rootPath, "subs", name));
      if (!(await dir.exists())) {
        _logger.warning(() => "unrecongnised 'directory': ${dir.path}");
      } else {
        var nodes = <Node>[];
        var nodeConfigs = dir.list();
        var sourceConfFile = new File(_p.join(dir.path, "_source.txt"));
        var sourceConf = (await sourceConfFile.exists())
            ? (await sourceConfFile.readAsString())
            : null;
        await for (var e in nodeConfigs) {
          var stat = await e.stat();
          if (stat.type == FileSystemEntityType.file &&
              e.path.endsWith('.yaml') &&
              !(_p.basename(e.path).startsWith('_'))) {
            _logger.fine(() => "load() hit: $e");
            var configContent = await (new File(e.path)).readAsString();
            var config = loadYaml(configContent) as YamlMap;
            _logger.fine(() => "load() read content: $config");
            var nodeConfig = ((config["config"] as YamlMap) ?? {})
                .map<String, dynamic>((key, value) => new MapEntry(key, value));
            nodes.add(new Node(config['name'], config['type'], nodeConfig));
          }
        }
        _subscriptions[name] = new NodeSubscription(name, sourceConf, nodes);
      }
    }
  }

  String get subscriptionDirectoryPath => _p.join(rootPath, "subs");

  Future save({bool rebuild = false}) async {
    Map<String, String> ramfs = {};
    List<String> needToCreateDirs = [];
    if (!(await (new Directory(subscriptionDirectoryPath)).exists())) {
      needToCreateDirs.add(subscriptionDirectoryPath);
    }
    for (var name in subscriptions.keys) {
      if (!(await (new Directory(_p.join(subscriptionDirectoryPath, name))
          .exists()))) {
        needToCreateDirs.add(subscriptionDirectoryPath);
      }
    }
    var subsFileContent =
        generateSubsFile(1, subscriptions.keys.toList(growable: false));
    ramfs[subsConfPath] = json2yaml(subsFileContent);
    var subsDirPath = subscriptionDirectoryPath;
    var localSubsNames = await (new Directory(subsDirPath).list().toSet());
    for (var localSubName in localSubsNames) {
      if (!subscriptions.keys.contains(_p.basename(localSubName.path))) {
        localSubName.delete(recursive: true);
      }
    }
    if (!rebuild) {
      for (var key in _changes.keys) {
        var value = _changes[key];
        var path = _p.join(subscriptionDirectoryPath, key);
        var dir = new Directory(path);
        if (!await dir.exists()) {
          needToCreateDirs.add(dir.path);
        } else {
          var localNames = await dir.list().asyncMap((event) async {
            var content = await (new File(event.path).readAsString());
            return [
              event.path,
              (loadYaml(content) as YamlMap)['name'] as String
            ];
          }).toSet();
          for (var pair in localNames) {
            String localName = pair[1];
            if (!value.nodes.map((e) => e.name).contains(localName)) {
              var path = pair[0];
              await (new File(path).delete());
            }
          }
        }
        var nodeFiles = value.genNodeFiles().map(
            (key, value) => new MapEntry(_p.join(path, key), json2yaml(value)));
        ramfs.addAll(nodeFiles);
        ramfs[_p.join(path, '_source.txt')] = value.source;
        _subscriptions[key] = _changes[value];
      }
    } else {
      await (new Directory(subscriptionDirectoryPath).delete(recursive: true));
      await (new Directory(subscriptionDirectoryPath).create());
      subscriptions.forEach((key, value) {
        var path = _p.join(subscriptionDirectoryPath, key);
        var nodeFiles = value.genNodeFiles().map(
            (key, value) => new MapEntry(_p.join(path, key), json2yaml(value)));
        ramfs.addAll(nodeFiles);
        ramfs[_p.join(path, '_source.txt')] = value.source;
      });
    }
    _changes.clear();
    _logger.info(
        "save(): generated ${ramfs.length} files, need to create ${needToCreateDirs.length} director{y,ies}");
    for (var p in needToCreateDirs) {
      await (new Directory(p)).create();
    }
    ramfs.removeWhere((key, value) => value == null);
    var result = await Future.wait(ramfs.map((key, value) {
      assert(value != null, "content must not be null");
      return new MapEntry(
          key,
          new File(key).writeAsString(value).then((value) => true,
              onError: (e, st) {
            _logger.shout(
                () =>
                    "save(): error while writing file $key with ${value.length} char",
                e,
                st);
            return false;
          }));
    }).values);
    var stat = result.reduce((value, element) => value && element);
    if (!stat) {
      _logger.shout(() => "save(): error have happened while writing files");
    }
  }

  Future deleteSubscription(String name) async {
    _subscriptions.remove(name);
    _changes.remove(name);
    await save();
  }

  Node lookupNode(String subscriptionName, String nodeName) {
    var subs = subscriptions[subscriptionName];
    if (subs == null) {
      return null;
    }
    var queried =
        subs.nodes.where((element) => element.name == nodeName).toList();
    if (queried.length == 1) {
      return queried[0];
    } else if (queried.length > 1) {
      _logger.shout("$subscriptionName have two or more node have same name!");
      return null;
    } else {
      return null;
    }
  }

  Iterable<NodeSubscription> collectSubscriptionHasSource() sync* {
    for (var x in subscriptions.values) {
      if (x.source != null) {
        yield x;
      }
    }
  }

  static Map<String, dynamic> generateSubsFile(
      int version, List<String> names) {
    return {'version': version, 'names': names};
  }

  static Future whenInit(String configRootPath) async {
    await new File("$configRootPath/subs.yaml")
        .writeAsString(json2yaml({'version': 1, 'names': []}));
  }
}

class NodeSubscription implements Comparable<NodeSubscription> {
  String name;
  String source;
  List<Node> nodes;

  NodeSubscription(this.name, this.source, this.nodes);

  Map<String, Map<String, dynamic>> genNodeFiles() {
    var map = <String, Map<String, dynamic>>{};
    for (var n in nodes) {
      map["${n.name}.yaml"] = n.gen();
    }
    return map;
  }

  @override
  String toString() {
    return "NodeSubscription(name: $name, source:$source, nodes.length: ${nodes.length})";
  }

  NodeSubscription clone() {
    return new NodeSubscription(
        name, source, nodes.map((e) => e.clone()).toList());
  }

  @override
  int compareTo(other) {
    return name.compareTo(other.name);
  }
}

class Node {
  String name;
  String type;
  Map<String, dynamic> config;

  Node(this.name, this.type, this.config);

  Map<String, dynamic> gen() {
    return {
      'name': name,
      'type': type,
      'config': config,
    };
  }

  ClashProxyConfGen toProxyConfGen() {
    if (type == "vmess") {
      return new ClashVmessConfGen(
          name: name,
          remoteAddress: config['remote_address'],
          remotePort: config['remote_port'],
          uuid: config['uuid'],
          alterId: config['alter_id'],
          cipher: config['cipher'],
          udp: config['udp'] ?? false,
          tls: config['tls'] ?? false,
          skipCertVerify: config['skip_cert_verify'] ?? false,
          network: config['network'],
          serverName: config['server_name'],
          wsPath: config['ws_path'],
          wsHeaders: Map.castFrom(config['ws_headers']),
          httpMethod: config['http_method'],
          httpHeaders: Map.castFrom(config['http_headers']),
          httpPaths: List.castFrom(config['http_paths']));
    } else {
      throw UnimplementedError();
    }
  }

  Node clone() {
    return new Node(
        name, type, config.map((key, value) => new MapEntry(key, value)));
  }
}

class ProxyNodesConfHolder {
  static ProxyNodesConf _value;

  /// Get the global [ProxyNodesConf].
  /// Create a new one from [ClearWaySettings.CONFIGURATION_FOLDER_PATH] if it's first time called.
  static Future<ProxyNodesConf> get() {
    if (_value != null) {
      return new Future.value(_value);
    } else {
      return ClearWaySetings.get(ClearWaySetings.CONFIGURATION_FOLDER_PATH)
          .then((p) => p != null
              ? p
              : new Future.error(new SettingItemRequired(
                  ClearWaySetings.CONFIGURATION_FOLDER_PATH)))
          .then((p) {
        _value = new ProxyNodesConf(p);
        return _value;
      });
    }
  }

  /// This method is for helping code get neat when you make sure there is a [ProxyNodesConf],
  /// because it nearly always exists in the application's lifeclyce.
  static ProxyNodesConf getSync() {
    assert(
        _value != null); // make sure it's a value when someone call the method
    return _value;
  }
}
