/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:clearway/conftool.dart';
import 'package:clearway/page_settings/tiles.dart';
import 'package:flutter/material.dart';

import 'clash/manger.dart';
import 'components/resource_provider.dart';

class ClearWayResources extends ResourceProvider {
  final ClashProcessManager clashProcessManager;
  final ProxyNodesConf proxyNodesConf;

  ClearWayResources(
      {Key key, Widget child, this.clashProcessManager, this.proxyNodesConf})
      : super(key: key, child: child);

  static ClearWayResources of(BuildContext context) =>
      ResourceProvider.of<ClearWayResources>(context);
}

class ResourceInjector<T extends ResourceProvider> extends StatelessWidget {
  final SingleValueTransformer<T, Widget> builder;

  ResourceInjector({Key key, this.builder}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var resource = ResourceProvider.of<T>(context);
    return builder(resource);
  }
}
