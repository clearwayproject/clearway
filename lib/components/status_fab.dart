/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'dart:async';
import 'dart:io';

import 'package:clearway/clash/config.dart';
import 'package:clearway/clash/controller.dart';
import 'package:clearway/clash/status_protocol.dart';
import 'package:clearway/common.dart';
import 'package:clearway/conftool.dart';
import 'package:clearway/global_res.dart';
import 'package:clearway/utils/clash_seesion.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as _p;

import 'fan_effect.dart';

class StatusFAB extends StatefulWidget {
  final StatusProtocol protocol;
  StatusFAB(this.protocol);

  @override
  StatusFABState createState() => StatusFABState(protocol);
}

class StatusFABState extends State<StatusFAB> {
  final StatusProtocol protocol;
  WorkingStatus status = WorkingStatus.NOT_WORKING;
  StreamSubscription<WorkingStatus> subscription;
  bool isInShutdown = false;
  StatusFABState(this.protocol);

  @override
  void initState() {
    super.initState();
    subscription = protocol.statusEvents.listen((event) {
      setState(() {
        status = protocol.currentStatus;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    status = ClearWayResources.of(context).clashProcessManager.currentStatus;
    return FloatingActionButton(
      child: FanEffect(
          key: ValueKey<WorkingStatus>(status),
          enabled: status == WorkingStatus.WORKING,
          child: Icon(Icons.toys),
          slow: isInShutdown),
      onPressed: () {
        if (status != WorkingStatus.NOT_WORKING) {
          protocol.stop();
        } else {
          var res = ClearWayResources.of(context);
          var man = res.clashProcessManager;
          var session = ClashSessionHolder.create();
          session.setProcessManager(man);
          new Future(() async {
            await session.doReady();
            session.checkManualy();
            try {
              await session.start();
            } on RemoteClashException catch (e) {
              showRemoteClashExceptionWithSnackBar(context, e);
            } on LookupFail catch (e) {
              Scaffold.of(context).showSnackBar(SnackBar(
                content: Text(
                    "Could not found node \"${e.nodeName}\" from \"${e.subscriptionName}\""),
              ));
            }
          });
        }
      },
    );
  }

  showRemoteClashExceptionWithSnackBar(
      BuildContext context, RemoteClashException e) {
    Scaffold.of(context).showSnackBar(new SnackBar(
        content: Text(e.shortMessage),
        action: SnackBarAction(
          label: "Details",
          onPressed: () {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text("Details"),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(2))),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(e.message),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          "Remote message",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Container(
                            width: double.infinity,
                            child: Text(
                              e.remoteMessage,
                            ),
                            margin: EdgeInsets.all(2),
                            color: Colors.grey.shade300)
                      ],
                    ),
                    actions: [
                      FlatButton(
                        child: Text("OK"),
                        onPressed: () => Navigator.of(context).pop(),
                      )
                    ],
                  );
                });
          },
        )));
  }
}
