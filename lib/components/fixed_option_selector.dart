/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:flutter/material.dart';

typedef Widget OptionWidgetBuilder<T>(T item);
typedef void SelectedChangedRecvicer<S extends State>(S state);

class FixedOptionSelector<T> extends StatefulWidget {
  final List<T> items;
  final OptionWidgetBuilder<T> builder;
  final bool mutiple;
  final List<T> initialSelected;
  final SelectedChangedRecvicer<FixedOptionSelectorState<T>> onSelectedChanged;

  FixedOptionSelector(
      {Key key,
      this.items,
      this.builder,
      this.mutiple: true,
      this.initialSelected,
      this.onSelectedChanged})
      : super(key: key);

  @override
  FixedOptionSelectorState<T> createState() =>
      FixedOptionSelectorState<T>(this);
}

class FixedOptionSelectorState<T> extends State<FixedOptionSelector> {
  final FixedOptionSelector<T> selector;
  Map<Widget, T> mapping;
  FixedOptionSelectorState(this.selector) {
    mapping = {};
    for (var x in selector.items) {
      mapping[selector.builder(x)] = x;
    }
  }

  List<T> selected;

  @override
  void initState() {
    super.initState();
    if (selected == null) {
      selected = selector.initialSelected ?? [];
    }
  }

  @override
  Widget build(BuildContext context) {
    if (selector.mutiple) {
      throw new UnimplementedError();
    } else {
      return PopupMenuButton<T>(
        padding: EdgeInsets.only(left: 16, right: 16),
        itemBuilder: (context) {
          var menuItems = <PopupMenuItem<T>>[];
          for (var widget in mapping.keys) {
            menuItems.add(PopupMenuItem(
              child: widget,
              value: mapping[widget],
            ));
          }
          return menuItems;
        },
        onSelected: (value) {
          setState(() {
            selected = [value];
          });
          selector.onSelectedChanged(this);
        },
        initialValue: selected[0],
        child: Row(
          children: [
            selector.builder(selected[0]),
            Icon(Icons.arrow_drop_down)
          ],
        ),
      );
    }
  }
}

class FixedOptionSelectorCard<T> extends StatelessWidget {
  final Widget title;
  final Widget description;
  final List<T> items;
  final OptionWidgetBuilder<T> builder;
  final bool mutiple;
  final List<T> initialSelected;
  final SelectedChangedRecvicer<FixedOptionSelectorState> onSelectedChanged;

  FixedOptionSelectorCard(
      {Key key,
      this.title,
      this.description,
      this.items,
      this.builder,
      this.mutiple,
      this.initialSelected,
      this.onSelectedChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(children: [
        Expanded(
          flex: 3,
          child: ListTile(
            title: title,
            subtitle: description,
          ),
        ),
        Flexible(
          flex: 0,
          child: FixedOptionSelector(
            items: items,
            builder: builder,
            mutiple: mutiple,
            initialSelected: initialSelected,
            onSelectedChanged: onSelectedChanged,
          ),
        )
      ]),
      padding: EdgeInsets.only(bottom: 16, right: 16),
    );
  }
}
