/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'dart:async';

import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';

class AsyncWidgetForceRebuildEvent {
  final String scope;
  final AsyncWidgetState state;
  AsyncWidgetForceRebuildEvent({this.scope, this.state});
}

typedef Future<Widget> AsyncWidgetComputation();

class AsyncWidget extends StatefulWidget {
  static final EventBus selfBus = new EventBus();
  final String scope;
  final Widget placeholder;
  final AsyncWidgetComputation computation;

  AsyncWidget({Key key, this.placeholder, this.computation, this.scope})
      : super(key: key);

  @override
  AsyncWidgetState createState() =>
      new AsyncWidgetState(this.placeholder, this.computation, this.scope);

  static void refresh({String scope, AsyncWidgetState state}) {
    AsyncWidget.selfBus
        .fire(new AsyncWidgetForceRebuildEvent(scope: scope, state: state));
  }
}

class AsyncWidgetState extends State<AsyncWidget> {
  static final _logger = new Logger("_AsyncWidgetState");
  final Widget placeholder;
  final AsyncWidgetComputation computation;
  final String scope;
  AsyncWidgetState(this.placeholder, this.computation, this.scope);

  StreamSubscription rebuildEventsSubscription;

  Widget result;

  @override
  void initState() {
    super.initState();
    rebuildEventsSubscription =
        AsyncWidget.selfBus.on<AsyncWidgetForceRebuildEvent>().listen((event) {
      if (event.scope != null && event.scope != this.scope) return;
      if (event.state != null && event.state != this) return;
      _logger.fine("$this(scoped $scope): start: $computation");
      computation().then((value) => setState(() {
            _logger.fine(
                "$this(scoped $scope): done: $computation, result: $value");
            result = value;
          }));
    });
    if (result == null) {
      AsyncWidget.refresh(state: this);
    }
  }

  @override
  void dispose() {
    super.dispose();
    rebuildEventsSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    if (result != null) {
      return result;
    } else {
      return placeholder;
    }
  }
}
