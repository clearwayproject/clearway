/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:flutter/material.dart';

class CardPrefaceTitle extends StatelessWidget {
  final String text;
  CardPrefaceTitle(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(children: [
        Text(
          text,
          textScaleFactor: 1.25,
          style: TextStyle(fontWeight: FontWeight.bold),
        )
      ]),
      padding: EdgeInsets.only(left: 16, top: 16, bottom: 8),
    );
  }
}
