/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'package:flutter/material.dart';

class AutoScaffold extends StatelessWidget {
  final PreferredSizeWidget appBar;
  final Widget drawer;
  final Widget body;
  final Widget floatingActionButton;

  const AutoScaffold(
      {Key key, this.appBar, this.drawer, this.body, this.floatingActionButton})
      : super(key: key);

  int chooseBodyPanelFlex(double width) {
    if (width <= 720) {
      return 2;
    } else if (width <= 960) {
      return 3;
    } else if (width <= 1280) {
      return 4;
    } else {
      return 5;
    }
  }

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    if (media.width > 600) {
      return Row(
        children: [
          Flexible(child: drawer),
          Flexible(
              flex: 0,
              child: const VerticalDivider(
                width: 0.1,
              )),
          Expanded(
            flex: chooseBodyPanelFlex(media.width),
            child: Scaffold(
              appBar: appBar,
              body: body,
              floatingActionButton: floatingActionButton,
            ),
          )
        ],
      );
    } else {
      return Scaffold(
        drawer: drawer,
        appBar: appBar,
        body: body,
        floatingActionButton: floatingActionButton,
      );
    }
  }
}
