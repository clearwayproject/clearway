/*
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import 'dart:math' as math;
import 'package:flutter/material.dart';

class FanEffect extends StatefulWidget {
  final bool enabled;
  final Widget child;
  final bool slow;
  FanEffect({Key key, this.enabled, this.child, this.slow}) : super(key: key);

  _FanEffectState createState() =>
      _FanEffectState(this.enabled, this.child, this.slow);
}

class _FanEffectState extends State<FanEffect>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  final bool enabled;
  final Widget child;
  final bool slow;

  _FanEffectState(this.enabled, this.child, this.slow);

  @override
  void initState() {
    _controller =
        AnimationController(duration: const Duration(seconds: 1), vsync: this)
          ..repeat();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (enabled) {
      return AnimatedBuilder(
        animation: _controller,
        child: this.child,
        builder: (context, child) => Transform.rotate(
          angle: _controller.value * (slow ? 0.1 : 3.5) * math.pi,
          child: child,
        ),
      );
    } else {
      return child;
    }
  }
}
