# Clash RESTFUL HTTP API
- Source: http://clash.gitbook.io/doc/restful-api
- This document is for correct some wrong from source

## Proxies
### `PUT /proxies/:name` Switch proxy in selector
The `name` in path is the selector name.  
Body:
````js
{
    "name": "", // Proxy name
}
````
Result:
- 404 NOT FOUND: The proxy not found
- 400 BAD REQUEST:
  ````js
  {
      "error": "Format error", // Or "Proxy can't update", means the proxy switched to has wrong type
  }
  ````
- 204 NO CONTENT: Done

## Config
### `PUT /configs` Reload configuration file from given path
> It does not affect `external-controller` and `secret`.

Query Arguments:
- `force`: optional boolean, does need to change the `port` and the other items which have been applied


Body: JSON
````js
{
    "path": "/path/to/config", // The path of configuration file you want to apply
}
````
Results:
- 204 NO CONTENT: Done
- 400 BAD REQUEST: It will be responded if request have problems, you can look at the reponded body for tips
