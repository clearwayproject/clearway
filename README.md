# ClearWay
> "Lives always find its own exit."

ClearWay (or clearway, clearWay, Clearway if you like) is a beautiful proxy client for linux built on [Clash](https://github.com/Dreamacro/clash/).

## Build
### Requirement
For now, you need to get a in dev channel copy with linux support of flutter standard development kit. 
- To get flutter SDK, follow the steps on https://flutter.dev/docs/get-started/install
  - If you are in China, the page above have a way to help you get packages on pub fast.
- To enable linux support, follow the steps on https://flutter.dev/desktop
  - This application only require you enable the linux support


The third-parties' packages are described in `pubspec.yaml` and you don't need to get them manually.

### Steps
1. get packages from pub
````
flutter pub get
````
2. Run or Build
  - Use `flutter run -d linux` to run the application in debug mode on your computer
  - Use `flutter build linux` to build the package, the production will placed in `build/linux/<debug/release>/bundle`.
  - In `lib/static_config.dart` have some item
    - I suggest you set `DEBUG` to `false` (`const DEBUG = false;`) for production, it will turn off debug logging and selection in application.

## LICENSE
![GPL_v3_Logo](wiki/img/gplv3.png)  
The GNU GENERAL PUBLIC LICENSE, version 3. This project source code contains a copy of the full text, see `gpl-3.0.md`.
````
    ClearWay - Lives always find its own exit.
    Copyright (C) 2020 The ClearWay Contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
````
- If you want to join the contributors, make contribution and place your name in `CONTRIBUTORS`, you can place name with email or without email, such as `Bot <ai.bot@example.com>` or `Bot`. One person a line. Add the most important one if you have mutilple nickname, username or real name.
